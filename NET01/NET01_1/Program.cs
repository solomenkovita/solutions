﻿using System;
using NET01_1.Entities;
using NET01_1.Enums;

namespace NET01_1
{
    class Program
    {
        static void Main(string[] args)
        {
            var lesson = new Lesson {Description = "New lesson"};
            lesson.SetId();

            //init text material
            var textMaterial = new TextMaterial();
            textMaterial.SetId();

            var text = "Some article";
            textMaterial.Text = text;

            var textDescription = "Text about something";
            textMaterial.Description = textDescription;


            //init link material
            var linkMaterial = new ResourceLink();
            linkMaterial.SetId();

            var contentUri = "0-22-def";
            linkMaterial.ContentURI = contentUri;

            linkMaterial.Type = LinkType.Html;

            var linkDescription = "Link with some meterial";
            linkMaterial.Description = linkDescription;


            //init video material
            var videoMaterial = new VideoMaterial();
            videoMaterial.SetId();

            var videoUri = "0-12-abc";
            videoMaterial.VideoURI = videoUri;

            var imageUri = "1-12-abc";
            videoMaterial.ImageURI = imageUri;

            videoMaterial.VideoFormat = Format.Mp4;

            var videoDescription = "Video about something";
            videoMaterial.Description = videoDescription;


            //add materials and getting type of lesson
            Console.WriteLine("Lesson type: " + lesson.GetLessonType().ToString());

            lesson.Add(textMaterial);
            lesson.Add(linkMaterial);
            Console.WriteLine("Lesson type: " + lesson.GetLessonType().ToString());

            lesson.Add(videoMaterial);
            Console.WriteLine("Lesson type: " + lesson.GetLessonType().ToString());


            //check equals method
            var videoMaterial2 = new VideoMaterial();
            videoMaterial2.Id = videoMaterial.Id;
            //videoMaterial2.SetId();

            var videoUri2 = "0-12-abcde";
            videoMaterial2.VideoURI = videoUri2;

            var imageUri2 = "1-12-abcde";
            videoMaterial2.ImageURI = imageUri2;

            videoMaterial2.VideoFormat = Format.Avi;

            var videoDescription2 = "Video about something 2";
            videoMaterial2.Description = videoDescription2;

            Console.WriteLine(!videoMaterial.Equals(videoMaterial2)
                ? "videomaterials are not equal"
                : "videomaterials are equal");


            //work with version
            lesson.SetVersion(1,2,3,4);
            Console.WriteLine("Lesson's version is: " + lesson.GetVersion());


            //clone lesson
            Lesson newLesson = (Lesson)lesson.Clone();
            Console.WriteLine("new lesson's version is: " + newLesson.GetVersion());
        }
    }
}
