﻿using System;
using NET01_1.Entities;

namespace NET01_1
{
    internal static class EntityHelper
    {
        public static void SetId(this Entity a)
        {
            if (a != null) a.Id = Guid.NewGuid();
        }
    }
}
