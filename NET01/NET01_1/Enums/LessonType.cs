﻿namespace NET01_1.Enums
{
    internal enum LessonType
    {
        VideoLesson,
        TextLesson
    }
}