﻿namespace NET01_1.Enums
{
    internal enum LinkType
    {
        Unknown,
        Html,
        Image,
        Audio,
        Video
    }
}