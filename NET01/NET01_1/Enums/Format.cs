﻿namespace NET01_1.Enums
{
    internal enum Format
    {
        Unknown,
        Avi,
        Mp4,
        Flv
    }
}