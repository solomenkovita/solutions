﻿namespace NET01_1
{
    internal interface IVersionable
    {
        ushort Major { get;}
        ushort Minor { get;}
        ushort Build { get;}
        ushort Revision { get;}

        string GetVersion();
        void SetVersion(ushort major, ushort minor, ushort build, ushort revision);
    }
}
