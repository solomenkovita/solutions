﻿using System;
using System.Text;
using NET01_1.Enums;

namespace NET01_1.Entities
{
    internal class VideoMaterial : Material, IVersionable
    {
        private ushort[] version = new ushort[4];
        private string videoURI;

        public ushort Major => version[0];
        public ushort Minor => version[1];
        public ushort Build => version[2];
        public ushort Revision => version[3];

        public string VideoURI
        {
            get => videoURI;
            set => videoURI = value ?? throw new ArgumentOutOfRangeException();
        }
        public string ImageURI { get; set; }
        public Format VideoFormat { get; set; }

        public string GetVersion()
        {
            var sb = new StringBuilder();
            foreach (var v in version)
                sb.Append(v.ToString());
            return sb.ToString();
        }

        public void SetVersion(ushort major, ushort minor, ushort build, ushort revision)
        {
            version[0] = major;
            version[1] = minor;
            version[2] = build;
            version[3] = revision;
        }

        public override Material Clone()
        {
            var arr = new ushort[4];
            version.CopyTo(arr, 0);
            return new VideoMaterial
            {
                Id = Id,
                Description = Description,
                ImageURI = ImageURI,
                VideoURI = VideoURI,
                VideoFormat = VideoFormat,
                version = arr
            };
        }
    }
}
