﻿namespace NET01_1.Entities
{
    internal abstract class Material: Entity
    {
        public abstract Material Clone();
    }
}