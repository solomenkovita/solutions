﻿using System;
using NET01_1.Enums;

namespace NET01_1.Entities
{
    internal class ResourceLink : Material
    {
        private string contentURI;
        public string ContentURI
        {
            get => contentURI;
            set => contentURI = value ?? throw new ArgumentOutOfRangeException();
        }

        public LinkType Type { get; set; }

        public override Material Clone()
        {
            return new ResourceLink()
            {
                Id = Id,
                Description = Description,
                ContentURI = ContentURI,
                Type = Type
            };
        }
    }
}