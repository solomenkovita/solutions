﻿using System;

namespace NET01_1.Entities
{
    internal abstract class Entity
    {
        private string description;
        public string Description
        {
            get => description;
            set =>
                description = !string.IsNullOrEmpty(value) && value.Length <= 256
                    ? value
                    : throw new ArgumentOutOfRangeException();
        }

        public Guid Id { get; set; }

        public override string ToString() => Description;

        public override bool Equals(object obj) => obj is Entity obj1 && Id.Equals(obj1.Id);

        public override int GetHashCode() => Id.GetHashCode();
    }
}
