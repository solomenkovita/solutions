﻿using System;
using System.Text;
using NET01_1.Enums;

namespace NET01_1.Entities
{
    internal class Lesson : Entity, IVersionable, ICloneable
    {
        private Material[] items = new Material[0];
        private readonly ushort[] version = new ushort[4];

        public ushort Major => version[0];
        public ushort Minor => version[1];
        public ushort Build => version[2];
        public ushort Revision => version[3];

        public void Add(Material item)
        {
            if (item == null) return;

            Array.Resize(ref items, items.Length + 1);
            items[^1] = item;
        }

        public LessonType GetLessonType()
        {
            foreach (var item in items)
                if (item is VideoMaterial)
                    return Enums.LessonType.VideoLesson;
            return Enums.LessonType.TextLesson;
        }

        public object Clone()
        {
            var newLesson = new Lesson();
            var newItems = new Material[items.Length];
            for (var i = 0; i < items.Length; i++) newItems[i] = items[i].Clone();
            newLesson.items = newItems;

            version.CopyTo(newLesson.version, 0);
            return newLesson;
        }

        public string GetVersion()
        {
            var sb = new StringBuilder();
            foreach (var v in version)
                sb.Append(v.ToString());
            return sb.ToString();
        }

        public void SetVersion(ushort major, ushort minor, ushort build, ushort revision)
        {
            version[0] = major; 
            version[1] = minor;
            version[2] = build;
            version[3] = revision;
        }
    }
}