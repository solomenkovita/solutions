﻿using System;

namespace NET01_1.Entities
{
    internal class TextMaterial : Material
    {
        private string text;
        public string Text
        {
            get => text;
            set => text = value != null && value.Length <= 10000 ? value : throw new ArgumentOutOfRangeException();
        }

        public override Material Clone()
        {
            return new TextMaterial()
            {
                Id = Id,
                Description = Description,
                Text = Text
            };
        }
    }
}