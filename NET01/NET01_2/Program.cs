﻿using System;
using NET01_2.Matrixes;

namespace NET01_2
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Input matrix _size (only 1 parameter): ");
                var size = int.Parse(Console.ReadLine());
                if (size == 0)
                    return;

                Console.WriteLine("Input WORDS for diagonal elements:");
                var str1 = Console.ReadLine()
                    ?.Split(new char[] {' ', '\n', '\t'}, StringSplitOptions.RemoveEmptyEntries);
                if (str1 == null || str1.Length == 0)
                    throw new ArgumentException("Empty string!!!");

                var dm = new DiagonalMatrix<string>(Math.Min(size, str1.Length));

                for (var i = 0; i < str1.Length; i++)
                    dm[i, i] = str1[i];
                Console.WriteLine("Your matrix is: \r\n" + dm);

                dm.ElementChanged += delegate (object sender, ElementChangedEventArgs<string> e) { Console.WriteLine("Change! (anonymous method)"); };
                dm.ElementChanged += OnElementChanged1;
                dm.ElementChanged += (sender, e) => { Console.WriteLine("Change! (lambda method)"); };
                dm[0, 0] = "SSS";
                Console.WriteLine(dm);

                Console.WriteLine("Input DOUBLE matrix (use ','):");
                var str2 = Console.ReadLine()
                    ?.Split(new char[] { ' ', '\n', '\t' }, StringSplitOptions.RemoveEmptyEntries);
                if (str2 == null || str2.Length == 0)
                    throw new ArgumentException("Empty string!!!");

                var num = str2.Length / size;
                if (num < size)
                    throw new ArgumentException("Wrong amount of elements");

                var sm = new SquareMatrix<double>(size);

                for (var i = 0; i < size; i++)
                    for (var j = 0; j < size; j++)
                        sm[i, j] = double.Parse(str2[i * size + j]);
                Console.WriteLine("Your matrix is: \r\n" + sm);

                sm.ElementChanged += delegate (object sender, ElementChangedEventArgs<double> e) { Console.WriteLine("Change! (anonymous method)"); };
                sm.ElementChanged += OnElementChanged2;
                sm.ElementChanged += (sender, e) => { Console.WriteLine("Change! (lambda method)"); };
                sm[0, 0] = 2.55;
                Console.WriteLine(sm);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void OnElementChanged1(object sender, ElementChangedEventArgs<string> e)
        {
            Console.WriteLine("Change! (simple method)");
        }

        private static void OnElementChanged2(object sender, ElementChangedEventArgs<double> e)
        {
            Console.WriteLine("Change! (simple method)");
        }
    }
}