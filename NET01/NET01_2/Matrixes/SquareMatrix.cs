﻿using System;
using System.Text;

namespace NET01_2.Matrixes
{
    /// <summary>
    /// Class for square matrix
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SquareMatrix<T>
    {
        /// <summary>
        /// Array for matrix elemens
        /// </summary>
        protected T[] Data;

        /// <summary>
        /// Field for matrix _size
        /// </summary>
        protected int _size;

        /// <summary>
        /// Property for _size of matrix
        /// </summary>
        public int Size => _size;

        /// <summary>
        /// Indexer to get matrix elements
        /// </summary>
        public virtual T this[int i, int j]
        {
            get
            {
                CheckIndexes(i, j);
                return Data[i * Size + j];
            }
            set
            {
                CheckIndexes(i, j);
                var index = i * Size + j;

                var oldValue = Data[index];
                if (Equals(oldValue, value)) return;
                Data[index] = value;
                OnElementChanged(new ElementChangedEventArgs<T>(i, j, oldValue, value));
            }
        }

        /// <summary>
        /// Base constructor
        /// </summary>
        public SquareMatrix() { }

        /// <summary>
        /// Constructor with parameters
        /// </summary>
        /// <param name="size">_size of matrix</param>
        public SquareMatrix(int size)
        {
            if (size < 0)
                throw new IndexOutOfRangeException(nameof(size));
            Data = new T[size*size];
            _size = size;
        }

        /// <summary>
        /// Event for element changing
        /// </summary>
        public event EventHandler<ElementChangedEventArgs<T>> ElementChanged;

        /// <summary>
        /// Raise ElementChanged event
        /// </summary>
        protected virtual void OnElementChanged(ElementChangedEventArgs<T> value)
        {
            ElementChanged?.Invoke(this, value);
        }

        /// <summary>
        /// Check if indexes are out of range
        /// </summary>
        protected void CheckIndexes(int i, int j)
        {
            if (i < 0 || i >= Size) throw new IndexOutOfRangeException(nameof(i));
            if (j < 0 || j >= Size) throw new IndexOutOfRangeException(nameof(j));
        }

        /// <summary>
        /// Method to convert matrix to string
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var str = new StringBuilder();
            for (var i = 0; i < Size; i++)
            {
                for (var j = 0; j < Size; j++)
                {
                    str.Append(this[i, j] == null ? "null" : this[i, j].ToString());
                    str.Append("\t");
                }
                str.AppendLine();
            }
            return str.ToString();
        }
    }
}