﻿using System;
using System.Text;

namespace NET01_2.Matrixes
{
    /// <summary>
    /// Class for Diagonal matrix
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class DiagonalMatrix<T> : SquareMatrix<T>
    {
        /// <summary>
        /// Indexer to get matrix elements
        /// </summary>
        public override T this[int i, int j]
        {
            get
            {
                CheckIndexes(i, j);
                return i != j ? default : Data[i];
            }
            set
            {
                CheckIndexes(i, j);
                if (i != j) return;

                var oldValue = Data[i];
                if (Equals(oldValue, value)) return;
                Data[i] = value;
                OnElementChanged(new ElementChangedEventArgs<T>(i, j, oldValue, value));
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="size">_size of matrix</param>
        public DiagonalMatrix(int size)
        {
            if (size < 0)
                throw new IndexOutOfRangeException(nameof(size));
            Data = new T[size];
            _size = Data.Length;
        }
    }
}