﻿using System;

namespace NET01_2
{
    /// <summary>
    ///  Class for ElementChanged event
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ElementChangedEventArgs<T> : EventArgs
    {
        public int I { get; }
        public int J { get; }
        public T OldValue { get; }
        public T NewValue { get; }

        public ElementChangedEventArgs(int i, int j, T oldValue, T newValue)
        {
            I = i;
            J = j;
            OldValue = oldValue;
            NewValue = newValue;
        }
    }
}