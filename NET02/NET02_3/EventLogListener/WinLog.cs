﻿using System;
using System.Diagnostics;
using Listener;

namespace EventLogListener
{
    public class WinLog : IListener
    {
        private string id;

        public string Id
        {
            get => id;
            set
            {
                CheckValue(value);
                id = value;
            }
        }

        public void WriteMessage(string message, LogLevel logLevel)
        {
            CheckValue(message);
            CheckValue(logLevel);

            EventLogEntryType levelLogEntryType;

            switch (logLevel)
            {
                case LogLevel.Error:
                    levelLogEntryType = EventLogEntryType.Error;
                    break;
                case LogLevel.Info:
                    levelLogEntryType = EventLogEntryType.Information;
                    break;
                case LogLevel.Warn:
                    levelLogEntryType = EventLogEntryType.Warning;
                    break;
                case LogLevel.Debug:
                    levelLogEntryType = EventLogEntryType.SuccessAudit;
                    break;
                case LogLevel.Fatal:
                    levelLogEntryType = EventLogEntryType.FailureAudit;
                    break;
                case LogLevel.Trace:
                    levelLogEntryType = EventLogEntryType.SuccessAudit;
                    break;
                default:
                    throw new ArgumentException("Unknown log level");
            }

            using (var eventLog = new EventLog())
            {
                eventLog.Source = "Application";
                eventLog.WriteEntry(message, levelLogEntryType, int.Parse(Id));
            }
        }

        private void CheckValue(object value)
        {
            switch (value)
            {
                case null:
                    throw new ArgumentNullException(nameof(value));
                case string s when string.IsNullOrEmpty(s):
                    throw new ArgumentException(nameof(value));
            }
        }
    }
}
