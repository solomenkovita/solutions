﻿using System;
using System.IO;
using Listener;
using Microsoft.Office.Interop.Word;

namespace WordListener
{
    public class Word : IListener
    {
        private string path;
        private string filename;
        private string fontsize;
        private string fontname;

        public string Path
        {
            get => path;
            set
            {
                CheckValue(value);
                path = value;
            }
        }

        public string Filename
        {
            get => filename;
            set
            {
                CheckValue(value);
                filename = value;
            }
        }

        public string Fontsize
        {
            get => fontsize;
            set
            {
                CheckValue(value);
                fontsize = value;
            }
        }

        public string Fontname
        {
            get => fontname;
            set
            {
                CheckValue(value);
                fontname = value;
            }
        }

        public void WriteMessage(string message, LogLevel logLevel)
        {
            CheckValue(message);
            CheckValue(logLevel);

            var dir = System.IO.Path.Combine(Directory.GetCurrentDirectory(), path);
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);
            var str = System.IO.Path.Combine(dir, filename);

            var app = new Application { ShowAnimation = false, Visible = false };
            Document document;
            
            if (!File.Exists(str))
            {
                document = app.Documents.Add();
                document.SaveAs2(str);
                document.Close();
                app.Quit();
            }
            
            document = app.Documents.Open(str);
            var p = document.Paragraphs.Add();
            p.Range.Text = DateTime.Now.ToString() + " " + logLevel + ": " + message;
            p.Range.Font.Size = float.Parse(Fontsize);
            p.Range.Font.Name = Fontname;
            switch (logLevel)
            {
                case LogLevel.Debug:
                    p.Range.Font.Color = WdColor.wdColorGreen;
                    break;
                case LogLevel.Warn:
                    p.Range.Font.Color = WdColor.wdColorYellow;
                    break;
                case LogLevel.Error:
                    p.Range.Font.Color = WdColor.wdColorRed;
                    break;
                case LogLevel.Fatal:
                    p.Range.Font.Color = WdColor.wdColorSkyBlue;
                    break;
                case LogLevel.Info:
                    p.Range.Font.Color = WdColor.wdColorPink;
                    break;
                case LogLevel.Trace:
                    p.Range.Font.Color = WdColor.wdColorViolet;
                    break;
            }
            document.Paragraphs.Add();
            document.Save();
            document.Close();
            app.Quit();
        }

        private void CheckValue(object value)
        {
            switch (value)
            {
                case null:
                    throw new ArgumentNullException(nameof(value));
                case string s when string.IsNullOrEmpty(s):
                    throw new ArgumentException(nameof(value));
            }
        }
    }
}