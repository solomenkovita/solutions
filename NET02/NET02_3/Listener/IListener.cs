﻿namespace Listener
{
    public interface IListener
    {
        void WriteMessage(string message, LogLevel logLevel);
    }
}
