﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using Listener;
using Logger.Attributes;
using Logger.Config;

namespace Logger
{
    public class Logger
    {
        private readonly List<IListener> _listeners = new List<IListener>();

        public LogLevel MinLevel { get; set; }

        public Logger()
        {
            StartLog();
        }

        public void Trace(string message)
        {
            WriteMessage(message, LogLevel.Trace);
        }

        public void Debug(string message)
        {
            WriteMessage(message, LogLevel.Debug);
        }

        public void Info(string message)
        {
            WriteMessage(message, LogLevel.Info);
        }

        public void Warn(string message)
        {
            WriteMessage(message, LogLevel.Warn);
        }

        public void Error(string message)
        {
            WriteMessage(message, LogLevel.Error);
        }

        public void Fatal(string message)
        {
            WriteMessage(message, LogLevel.Fatal);
        }

        public void Track(object obj)
        {
            if (obj == null) throw new ArgumentNullException(nameof(obj));

            var type = obj.GetType();
            var attribute = type.GetCustomAttribute(typeof(TrackingEntity));
            if (attribute == null) return;

            foreach (var field in type.GetFields())
            {
                var atr = (TrackingProperty)field.GetCustomAttribute(typeof(TrackingProperty));
                if (atr == null) continue;
                var str = !string.IsNullOrEmpty(atr.Name) ? atr.Name : field.Name;
                Trace($"{str}={field.GetValue(obj)}");
            }

            foreach (var property in type.GetProperties())
            {
                var atr = (TrackingProperty) property.GetCustomAttribute(typeof(TrackingProperty));
                if (atr == null) continue;
                var str = !string.IsNullOrEmpty(atr.Name) ? atr.Name : property.Name;
                Trace($"{property.Name}={property.GetValue(obj)}");
            }
        }

        private void WriteMessage(string message, LogLevel logLevel)
        {
            if (string.IsNullOrEmpty(message)) throw new ArgumentException(nameof(message));

            if (logLevel < MinLevel) return;

            foreach (var logListener in _listeners) 
                logListener.WriteMessage(message, logLevel);
        }

        private void StartLog()
        {
            var item = ConfigurationManager.GetSection("logger");

            if (item is null) throw new ArgumentNullException(nameof(item));
            if (!(item is LoggerSection)) throw new ArgumentException(nameof(item));

            var config = (LoggerSection) item;
            MinLevel = (LogLevel) Enum.Parse(typeof(LogLevel), config.MinLevel);

            var listenersCollection = config.Listeners;
            foreach (Config.Listener listener in listenersCollection)
            {
                Type listenerType = Type.GetType(listener.Type);
                if (listenerType == null) continue;
                var logListener = (IListener)Activator.CreateInstance(listenerType);

                var properties = listener.Properties;
                foreach (Property property in properties)
                {
                    var currentProperty = listenerType.GetProperty(property.Name);
                    if (currentProperty != null) currentProperty.SetValue(logListener, property.Value);
                }

                _listeners.Add(logListener);
            }
        }
    }
}