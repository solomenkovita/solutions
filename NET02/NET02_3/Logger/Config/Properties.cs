﻿using System.Configuration;

namespace Logger.Config
{
    [ConfigurationCollection(typeof(Property), AddItemName = "property")]
    public class Properties : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement() => new Property();

        protected override object GetElementKey(ConfigurationElement element) => ((Property)element).Name;
    }
}
