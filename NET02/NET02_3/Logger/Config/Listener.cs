﻿using System.Configuration;

namespace Logger.Config
{
    public class Listener : ConfigurationElement
    {
        [ConfigurationProperty("type", IsRequired = true)]
        public string Type => (string)this["type"];

        [ConfigurationProperty("properties", IsRequired = false)]
        public Properties Properties => (Properties)base["properties"];
    }
}
