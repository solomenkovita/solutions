﻿using System.Configuration;

namespace Logger.Config
{
    [ConfigurationCollection(typeof(Listener), AddItemName = "listener")]
    public class Listeners : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement() => new Listener();

        protected override object GetElementKey(ConfigurationElement element) => ((Listener)element).Type;
    }
}
