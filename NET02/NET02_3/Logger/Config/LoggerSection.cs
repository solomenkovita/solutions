﻿using System.Configuration;

namespace Logger.Config
{
    public class LoggerSection : ConfigurationSection
    {
        [ConfigurationProperty("listeners", IsDefaultCollection = true)]
        public Listeners Listeners => (Listeners)base["listeners"];

        [ConfigurationProperty("minLevel", IsRequired = true)]
        public string MinLevel => (string)this["minLevel"];
    }
}
