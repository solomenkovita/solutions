﻿using System.Configuration;

namespace Logger.Config
{

    public class Property : ConfigurationElement
    {
        [ConfigurationProperty("name", IsRequired = true)]
        public string Name => (string)this["name"];

        [ConfigurationProperty("value", IsRequired = true)]
        public string Value => (string)this["value"];
    }
}
