﻿using System;

namespace Logger.Attributes
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class TrackingProperty : Attribute
    {
        public string Name { get; set; }

        public TrackingProperty() { }

        public TrackingProperty(string name) { }
    }
}
