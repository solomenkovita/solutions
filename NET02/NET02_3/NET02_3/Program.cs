﻿namespace NET02_3
{
    class Program
    {
        static void Main(string[] args)
        { 
            var logger = new Logger.Logger();
            logger.Error("some error");
            logger.Warn("some warning");

            var school = new School(1000, 67, "Gomel", false);
            logger.Track(school);
        }
    }
}
