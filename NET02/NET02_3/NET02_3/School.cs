﻿using Logger.Attributes;

namespace NET02_3
{
    [TrackingEntity]
    public class School
    {
        [TrackingProperty("studentsNum")] 
        private int studentsNum;

        [TrackingProperty("number")] 
        public int Number { get;}

        [TrackingProperty] 
        public string City { get; }

        public bool IsGymnasium { get; set; }

        public School(int students, int number, string city, bool gym)
        {
            studentsNum = students;
            Number = number;
            City = city;
            IsGymnasium = gym;
        }
    }
}
