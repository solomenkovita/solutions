﻿using System;
using System.IO;
using Listener;

namespace TextListener
{
    public class Text : IListener
    {
        private string filename;
        private string path;

        public string Path
        {
            get => path;
            set
            {
                CheckValue(value);
                path = value;
            }
        }

        public string FileName
        {
            get => filename;
            set
            {
                CheckValue(value);
                filename = value;
            }
        }

        private void CheckValue(object value)
        {
            switch (value)
            {
                case null:
                    throw new ArgumentNullException(nameof(value));
                case string s when string.IsNullOrEmpty(s):
                    throw new ArgumentException(nameof(value));
            }
        }
       
        public void WriteMessage(string message, LogLevel logLevel)
        {
            CheckValue(message);
            CheckValue(logLevel);

            var dir = System.IO.Path.Combine(Directory.GetCurrentDirectory(), path);
            if (!Directory.Exists(dir))
                Directory.CreateDirectory(dir);

            var str = System.IO.Path.Combine(dir, filename);
            if (!File.Exists(str))
            {
                using (File.Create(str));
                File.WriteAllText(str, DateTime.Now.ToString()+ " " + logLevel + ": "+ message + "\r\n");
            }
            else
            {
                File.AppendAllText(str, DateTime.Now.ToString() + " " + logLevel + ": " + message + "\r\n");
            }
        }
    }
}
