﻿using System;
using System.Net;
using System.Threading;
using NLog;

namespace NET02_4
{
    public class SiteTracking
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private SendEmail _sendEmail { get; }
        private SiteConfig _siteConfig { get; }
        private bool _cancelTracking = false;

        public SiteTracking(SendEmail sendEmail, SiteConfig siteConfig)
        {
            Utils.CheckValue(sendEmail);
            Utils.CheckValue(siteConfig);

            _sendEmail = sendEmail;
            _siteConfig = siteConfig;
        }

        public void StopTracking()
        {
            _cancelTracking = true;
        }

        public void StartTracking()
        {
            _cancelTracking = false;
            var thread = new Thread(Tracking);
            thread.Start();
        }

        private async void Tracking()
        {
            while (!_cancelTracking)
            {
                if (SiteTesting(_siteConfig.Address, _siteConfig.Timeout))
                    Logger.Info($" {_siteConfig.Address} is available");
                else
                    await _sendEmail.Send($"Site {_siteConfig.Address} is not available!",
                        "Site information", _siteConfig.Email, _siteConfig.Recipient);
                Thread.Sleep(_siteConfig.Interval);
            }
        }

        private bool SiteTesting(string address, int timeout)
        {
            var request = (HttpWebRequest) WebRequest.Create(new Uri(address));
            request.Timeout = timeout;
            try
            {
                var response = request.GetResponse();
                response.Close();
            }
            catch (Exception) { return false; }
            return true;
        }
    }
}