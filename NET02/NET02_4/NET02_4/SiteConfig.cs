﻿using System;

namespace NET02_4
{
    public class SiteConfig
    {
        public int Interval { get; }
        public int Timeout { get; }
        public string Address { get; }
        public string Email { get; }
        public string Recipient { get; } = "solomenkovita@gmail.com";

        public SiteConfig(int interval, int timeout, string address, string email)
        {
            Utils.CheckValue(interval);
            Utils.CheckValue(timeout);
            Utils.CheckValue(address);
            Utils.CheckValue(email);

            Email = email;
            Interval = interval;
            Timeout = timeout;
            Address = address;
        }
    }
}
