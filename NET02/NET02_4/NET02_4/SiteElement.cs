﻿using System.Configuration;

namespace NET02_4
{
    public class SiteElement : ConfigurationElement
    {
        [ConfigurationProperty("name", IsKey = true, IsRequired = true)]
        public string Address => (string)this["name"];

        [ConfigurationProperty("interval", IsRequired = true)]
        public int Interval => (int)this["interval"];

        [ConfigurationProperty("timeout", IsRequired = true)]
        public int Timeout => (int)this["timeout"];

        [ConfigurationProperty("email")]
        public string Email => (string)this["email"];
    }
}
