﻿using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;

namespace NET02_4
{
    public class Monitoring
    {
        private readonly FileSystemWatcher _watcher;
        private bool _isMonitoring;
        private readonly List<SiteTracking> _monitoringSites;
        private readonly List<SiteConfig> _siteConfig;

        public Monitoring()
        {
            _siteConfig = new List<SiteConfig>();
            _monitoringSites = new List<SiteTracking>();
            Start();

            _watcher = new FileSystemWatcher
            {
                Path = Directory.GetCurrentDirectory(),
                NotifyFilter = NotifyFilters.LastWrite,
                Filter = "NET02_4.exe.config"
            };

            _watcher.Changed += OnChanged;
            _watcher.EnableRaisingEvents = false;
        }

        private void Start()
        {
            var configManager = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            if (configManager.GetSection("SiteSection") is SiteSection siteSection)
            {
                for (var i = 0; i < siteSection.Sites.Count; i++)
                {
                    var siteSetting = new SiteConfig(siteSection.Sites[i].Interval, siteSection.Sites[i].Timeout,
                        siteSection.Sites[i].Address, siteSection.Sites[i].Email);

                    _siteConfig.Add(siteSetting);
                }
            }
        }

        private void OnChanged(object source, FileSystemEventArgs e)
        {
            StopMonitoring();
            ConfigurationManager.RefreshSection("SiteSection");
            Start();
            StartMonitoring();
        }

        public void StartMonitoring()
        {
            _watcher.EnableRaisingEvents = true;
            _isMonitoring = true;
            var sendEmail = new SendEmail();
            foreach (var site in _siteConfig.Select(siteConfig => new SiteTracking(sendEmail, siteConfig)))
            {
                _monitoringSites.Add(site);
                site.StartTracking();
            }
        }

        public void StopMonitoring()
        {
            if (!_isMonitoring) return;

            _watcher.EnableRaisingEvents = false;
            foreach (var site in _monitoringSites) site.StopTracking();

            _isMonitoring = false;
        }
    }
}