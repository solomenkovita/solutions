﻿using System.Configuration;

namespace NET02_4
{
    public class SiteSection : ConfigurationSection
    {
        [ConfigurationProperty("sites")]
        public SiteCollection Sites => (SiteCollection)base["sites"];
    }
}