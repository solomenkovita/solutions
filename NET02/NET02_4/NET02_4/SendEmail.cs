﻿using System;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace NET02_4
{
    public class SendEmail
    {
        public async Task Send(string message, string subject, string email, string recipient)
        {
            Utils.CheckValue(message);
            Utils.CheckValue(subject);
            Utils.CheckValue(email);

            var from = new MailAddress(email);
            var to = new MailAddress(recipient);

            var m = new MailMessage(from, to) {Subject = subject, Body = message};
            var smtp = new SmtpClient("smtp.mail.ru", 587)
            {
                Credentials = new NetworkCredential(email, "ks2000sha"), EnableSsl = true
            };
            await smtp.SendMailAsync(m);
        }
    }
}