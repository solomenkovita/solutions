﻿using System;
using System.Threading;

namespace NET02_4
{
    class Program
    {
        static void Main(string[] args)
        {
            using (new Mutex(false, "MyMutex", out var ex))
            {
                if (!ex)
                {
                    Console.WriteLine("App has been already run, press any key to close");
                    Console.ReadLine();
                    return;
                }

                var monitoring = new Monitoring();
                monitoring.StartMonitoring();
                Console.ReadLine();
                monitoring.StopMonitoring();
            }
        }
    }
}
