﻿using System.Configuration;

namespace NET02_4
{
    [ConfigurationCollection(typeof(SiteElement), AddItemName = "site")]
    public class SiteCollection : ConfigurationElementCollection
    {
        public SiteElement this[int index] => (SiteElement)BaseGet(index);

        protected override ConfigurationElement CreateNewElement() => new SiteElement();

        protected override object GetElementKey(ConfigurationElement element) => ((SiteElement)element).Address;

        protected override void BaseAdd(ConfigurationElement element) => BaseAdd(element, false);
    }
}