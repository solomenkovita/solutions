﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NET02_1
{
    public class Catalog : Dictionary<Isbn, Book>
    {
        public new Book this[Isbn key]
        {
            get => base[key];
            set
            {
                value.Isbn = key;
                base[key] = value;
            }
        }

        public IEnumerable<Book> GetBooks()
        {
            return Values.OrderByDescending(x => x.PublicationDate);
        }

        public IEnumerable<Book> GetBooks(string firstName, string lastName)
        {
            if (string.IsNullOrEmpty(firstName) && string.IsNullOrEmpty(lastName))
            {
                throw new ArgumentException("Must be non-empty");
            }

            return Values.Where(x => x.GetAuthors().Contains(new Author(firstName, lastName)));
        }

        public IEnumerable<(Author, int)> AuthorBook()
        {
            return Values.SelectMany(book => book.GetAuthors())
                .GroupBy(author => author)
                .Select(group => (group.Key, group.Count()));
        }

        public void Add(Book book)
        {
            Add(book.Isbn, book);
        }
    }
}
