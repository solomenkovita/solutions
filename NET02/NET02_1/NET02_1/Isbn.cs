﻿using System;
using System.Text.RegularExpressions;

namespace NET02_1
{
    public class Isbn
    {
        private Regex regex = new Regex("[0-9]{3}-*[0-9]{1}-*[0-9]{2}-*[0-9]{6}-*[0-9]{1}");

        private readonly string isbn;

        private Isbn(string Isbn)
        {
            if (string.IsNullOrEmpty(Isbn))
            {
                throw new ArgumentException("Must be non-empty", nameof(Isbn));
            }

            if (!regex.IsMatch(Isbn))
            {
                throw new ArgumentException(
                    $"Format of {nameof(Isbn)} must satisfy 'DDD-D-DD-DDDDDD-D' or 'DDDDDDDDDDDDD' where D is 0..9");
            }

            isbn = Isbn.Replace("-", "");
        }

        public override bool Equals(object obj) => obj is Isbn other && isbn.Equals(other.isbn);

        public override int GetHashCode() => isbn.GetHashCode();

        public override string ToString() => isbn;

        public static implicit operator Isbn(string str) => new Isbn(str);
    }
}
