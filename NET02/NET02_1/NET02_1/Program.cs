﻿using System;

namespace NET02_1
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var catalog = new Catalog();

                var book1 = new Book("1234567890123", "Alice’s Adventures in Wonderland", new DateTime(1865, 1, 1),
                    new Author("Lewis", "Carol"));
                var book2 = new Book("123-4-12-345678-9", "The Prince and the Pauper", new DateTime(1881, 1, 1),
                    new Author("Mark", "Twain"));
                var book3 = new Book("123-4-12-345678-0", "The Adventures of Tom Sawyer", new DateTime(1876, 1, 1),
                    new Author("Mark", "Twain"));

                catalog.Add(book1);
                catalog.Add(book2);
                catalog.Add(book3);

                Console.WriteLine(catalog["123-4-12-345678-9"]);

                catalog["111-1-11-111111-1"] = book1;

                Console.WriteLine("All books");
                foreach (var b in catalog.GetBooks())
                {
                    Console.WriteLine(b);
                }

                Console.WriteLine("Mark Twain books");
                foreach (var b in catalog.GetBooks("Mark", "Twain"))
                {
                    Console.WriteLine(b);
                }

                Console.WriteLine("Statistics:");
                foreach (var (a, b) in catalog.AuthorBook())
                {
                    Console.WriteLine($"{a} = {b}");
                }

                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
