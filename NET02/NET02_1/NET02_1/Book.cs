﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NET02_1
{
    public class Book
    {
        private string title;
        private readonly HashSet<Author> authors = new HashSet<Author>();

        public string Title
        {
            get => title;
            set =>
                title = !string.IsNullOrEmpty(value) && value.Length <= 1000
                    ? value
                    : throw new ArgumentException("Must be non-empty", nameof(title));
        }
        public Isbn Isbn { get;set; }
        public DateTime? PublicationDate { get; private set; }

        public Book(Isbn isbn, string title, DateTime publicationDate, params Author[] authors)
        {
            Isbn = isbn;
            Title = title;
            PublicationDate = publicationDate.Date;

            if (authors != null)
            {
                foreach (var author in authors)
                {
                    if (author != null)
                    {
                        this.authors.Add(new Author(author.FirstName, author.LastName));
                    }
                }
            }
        }

        public IEnumerable<Author> GetAuthors() => authors;

        public override string ToString()
        {
            var sb = new StringBuilder("Title: ");
            sb.AppendLine(Title);

            if (authors.Count > 0)
            {
                sb.AppendLine("Author(s):");
                foreach (var author in authors)
                {
                    sb.Append("   ").AppendLine(author.ToString());
                }
            }
            sb.AppendLine("Publication date: "+ PublicationDate.Value.ToShortDateString());
            return sb.ToString();
        }

        public override bool Equals(object obj) => obj is Book obj1 && obj1.Isbn.Equals(Isbn);

        public override int GetHashCode() => Isbn.GetHashCode();
    }
}
