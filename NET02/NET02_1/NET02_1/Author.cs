﻿using System;

namespace NET02_1
{
    public class Author
    {
        private string firstName;
        private string lastName;

        public string FirstName
        {
            get => firstName;
            set =>
                firstName = !string.IsNullOrEmpty(value) && value.Length <= 200
                    ? value.ToUpper()
                    : throw new ArgumentException("Must be non-empty", nameof(firstName));
        }

        public string LastName
        {
            get => lastName;
            set =>
                lastName = !string.IsNullOrEmpty(value) && value.Length <= 200
                    ? value.ToUpper()
                    : throw new ArgumentException("Must be non-empty", nameof(lastName));
        }

        public Author(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public override string ToString()
        {
            return firstName + " " + lastName;
        }

        public override int GetHashCode() => ToString().GetHashCode();

        public override bool Equals(object obj) => obj is Author obj1 && (firstName.Equals(obj1.firstName) && lastName.Equals(obj1.lastName));
    }
}