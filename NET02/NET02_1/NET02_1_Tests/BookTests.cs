﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NET02_1;

namespace NET02_1_Tests
{
    [TestClass]
    public class BookTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void BookSetWrongTitleLength()
        {
            var book = new Book("1234567890123", new string('a', 1001),
                new DateTime(1865, 1, 1), new Author("Lewis", "Carol"));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void BookSetTitleNull()
        {
            var book = new Book("1234567890123", null,
                new DateTime(1865, 1, 1), new Author("Lewis", "Carol"));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void BookTitleSetEmpty()
        {
            var book = new Book("1234567890123", "",
                new DateTime(1865, 1, 1), new Author("Lewis", "Carol"));
        }
    }
}
