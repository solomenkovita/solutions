﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NET02_1;

namespace NET02_1_Tests
{
    [TestClass]
    public class CatalogTests
    {
        [TestMethod]
        public void IsbnEquals()
        {
            var book1 = new Book("1234567890123", "Alice’s Adventures in Wonderland", new DateTime(1865, 1, 1), new Author("Lewis", "Carol"));
            var book2 = new Book("123-4-56-789012-3", "The Prince and the Pauper", new DateTime(1881, 1, 1), new Author("Mark", "Twain"));

            Assert.AreEqual(book1, book2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void IsbnWrongFormat()
        {
            var book = new Book("1234-567-89-0-123", "Alice’s Adventures in Wonderland", new DateTime(1865, 1, 1), new Author("Lewis", "Carol"));
        }

        [TestMethod]
        public void GetByKey()
        {
            var book = new Book("1234567890123", "Alice’s Adventures in Wonderland", new DateTime(1865, 1, 1), new Author("Lewis", "Carol"));
            var catalog = new Catalog { book };
            Assert.AreEqual(catalog["1234567890123"].Title, book.Title);
        }

        [TestMethod]
        public void GetByDifferentKeyForm()
        {
            var book = new Book("1234567890123", "Alice’s Adventures in Wonderland", new DateTime(1865, 1, 1), new Author("Lewis", "Carol"));
            var catalog = new Catalog { book };
            Assert.AreEqual(catalog["123-4-56-789012-3"].Title, book.Title);
        }
    }
}
