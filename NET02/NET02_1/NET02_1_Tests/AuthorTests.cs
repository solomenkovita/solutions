using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NET02_1;

namespace NET02_1_Tests
{
    [TestClass]
    public class AuthorTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void AuthorFirstNameWrongLength()
        {
            var author = new Author(new string('a', 300), "Ivanov");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void AuthorSetNull()
        {
            var author = new Author("Ivan", null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void AuthorSetEmpty()
        {
            var author = new Author("", "Ivanov");
        }
    }
}
