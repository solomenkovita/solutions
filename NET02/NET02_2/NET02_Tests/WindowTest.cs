﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NET02_2;

namespace NET02_2_Tests
{
    [TestClass]
    public class WindowTest
    {
        [TestMethod]
        public void CorrectWindow()
        {
            var window = new Window
            {
                Title = "main",
                Height = 200,
                Width = 400,
                Top = 20,
                Left = 10
            };
            Assert.IsTrue(window.IsCorrect());
        }

        [TestMethod]
        public void NotCorrectWindow()
        {
            var window = new Window
            {
                Title = "main",
                Height = 200,
                Width = 400,
                Top = null,
                Left = 10
            };
            Assert.IsFalse(window.IsCorrect());
        }

        [TestMethod]
        public void ChangeValueWidth()
        {
            var window = new Window
            {
                Title = "main",
                Height = 150,
                Width = null,
                Top = 20,
                Left = null
            };

            window.CorrectValues();
            Assert.AreEqual(window.Width, 400);
        }

        [TestMethod]
        public void ChangeValueTop()
        {
            var window = new Window
            {
                Title = "main",
                Height = 200,
                Width = 400,
                Top = null,
                Left = null
            };

            window.CorrectValues();
            Assert.AreEqual(window.Top, 0);
        }
    }
}
