﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NET02_2;

namespace NET02_2_Tests
{
    [TestClass]
    public class JsonRepositoryTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ConfigSetNull()
        {
            var jrepo = new JsonRepository(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void FilenameSetNull()
        {
            var jrepo = new JsonRepository(new Configurations());
            jrepo.Write(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void FilenameEmpty()
        {
            var jrepo = new JsonRepository(new Configurations());
            jrepo.Write("");
        }
    }
}
