﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NET02_2;

namespace NET02_2_Tests
{
    [TestClass]
    public class XmlParserTest
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void FilenameNull()
        {
            var xmlParser = new XmlParser();
            xmlParser.Read(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void FilenameEmpty()
        {
            var xmlParser = new XmlParser();
            xmlParser.Read("");
        }
    }
}
