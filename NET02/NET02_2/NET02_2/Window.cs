﻿namespace NET02_2
{
    public class Window
    {
        public string Title { get; set; }
        public int? Top { get; set; }
        public int? Left { get; set; }
        public int? Height { get; set; }
        public int? Width { get; set; }

        public bool IsCorrect()
        {
            if (Title == "main" && !(Top is null || Left is null || Height is null || Width is null))
            {
                return true;
            }
            return Title != "main";
        }

        public void CorrectValues()
        {
            Top ??= 0;
            Left ??= 0;
            Height ??= 150;
            Width ??= 400;
        }

        public override string ToString()
        {
            return
                $"\t{Title} ({NullToString(Top)}, {NullToString(Left)}, {NullToString(Height)}, {NullToString(Width)})";

            string NullToString(int? str)
            {
                return str == null ? "?" : str.ToString();
            }
        }
    }
}