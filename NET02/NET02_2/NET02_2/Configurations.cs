﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NET02_2
{
    public class Configurations
    {
        public List<User> Users { get; set; } = new List<User>();

        public List<User> NotCorrectLogins() => Users.Where(user => user.Windows != null || !user.IsCorrect()).ToList();

        public override string ToString()
        {
            var str = new StringBuilder();
            foreach (var user in Users.Where(user => user.Windows != null))
            {
                str.Append(user);
            }
            return str.ToString();
        }
    }
}