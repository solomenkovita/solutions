﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NET02_2
{
    public class User
    {
        public string Name { get; set; }
        public List<Window> Windows { get; set; } = new List<Window>();

        public bool IsCorrect() => Windows.Any(window => window.IsCorrect());

        public void MakeCorrectValues()
        {
            foreach (var w in Windows) w.CorrectValues();
        }

        public override string ToString()
        {
            var str = new StringBuilder();
            str.AppendLine($"Login: {Name}");
            foreach (var w in Windows) str.AppendLine(w.ToString());

            return str.ToString();
        }
    }
}