﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace NET02_2
{
    public class XmlParser
    {
        public Configurations Read(string filename)
        {
            if (string.IsNullOrEmpty(filename)) throw new ArgumentException();

            var doc = XDocument.Load(filename);
            var root = doc.Element("config");
            if (root == null) throw new InvalidOperationException("Root element 'config' is missing");

            var config = new Configurations
            {
                Users = root.Elements("login").Select(log => new User
                    {
                        Name = log.Attribute("name")?.Value,
                        Windows = log.Elements("window").Select(x => new Window
                        {
                            Title = x.Attribute("title")?.Value,
                            Height = TryParseNull(x.Element("height")?.Value),
                            Left = TryParseNull(x.Element("left")?.Value),
                            Top = TryParseNull(x.Element("top")?.Value),
                            Width = TryParseNull(x.Element("width")?.Value)
                        }).ToList()
                    }
                ).ToList()
            };
            return config;

            int? TryParseNull(string value)
            {
                if (value == null) return null;
                return int.Parse(value);
            }
        }
    }
}