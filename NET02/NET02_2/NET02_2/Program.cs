﻿using System;

namespace NET02_2
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var xmlParser = new XmlParser();
            var configurations = xmlParser.Read("config.xml");
            foreach (var user in configurations.NotCorrectLogins())
            {
                Console.WriteLine(user);
            }

            var jsonrepo = new JsonRepository(configurations);
            jsonrepo.Write("CONFIG.json");
            jsonrepo.WriteUser();

        }
    }
}