﻿using System;
using System.IO;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Unicode;

namespace NET02_2
{
    public class JsonRepository
    {
        private readonly JsonSerializerOptions options;
        private readonly Configurations config;
        private readonly string path;
        private readonly string folder = "Config";

        public JsonRepository(Configurations configurations)
        {
            config = configurations ?? throw new ArgumentNullException(nameof(config));
            path = Directory.GetCurrentDirectory();
            config.Users.ForEach(x => x.MakeCorrectValues());
            options = new JsonSerializerOptions
            {
                Encoder = JavaScriptEncoder.Create(UnicodeRanges.BasicLatin),
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                WriteIndented = true
            };
        }

        public void Write(string filename)
        {
            if (string.IsNullOrEmpty(filename)) throw new ArgumentException("Name of the file cannot be empty");

            config.Users.ForEach(x => x.MakeCorrectValues());
            filename = Path.Combine(path, filename);
            var json = JsonSerializer.Serialize(config, options);
            File.WriteAllText(filename, json);
        }

        public void WriteUser()
        {
            for (var i = 0; i < config.Users.Count; i++)
            {
                var newPath = Path.Combine(path, folder);
                if (!Directory.Exists(newPath))
                    Directory.CreateDirectory(newPath);
                var filename = Path.Combine(newPath, config.Users[i].Name + ".json");

                var json = JsonSerializer.Serialize(config.Users[i], options);
                File.WriteAllText(filename, json);
            }
        }
    }
}