﻿using System;
using NET01._2.Matrices;

namespace NET01._2
{
    public class Program
    {
        public static void Main()
        {
            var dm = new DiagonalMatrix<int>(3)
            {
                [0, 0] = 10,
                [1, 1] = 20,
                [2, 2] = 30
            };
            Console.WriteLine(dm);
            Console.ReadLine();

            var utm = new UpperTriangularMatrix<int>(3)
            {
                [0, 0] = 10,
                [1, 1] = 20,
                [1, 2] = 25,
                [2, 2] = 30
            };
            Console.WriteLine(utm);

            Console.ReadLine();
        }
    }
}