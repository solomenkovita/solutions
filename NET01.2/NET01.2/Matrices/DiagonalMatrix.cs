﻿namespace NET01._2.Matrices
{
    public class DiagonalMatrix<T> : SquareMatrix<T>
    {
        public DiagonalMatrix(int size) : base(size, size)
        {
        }

        protected override T GetValue(int i, int j) => i != j ? default : base.GetValue(i, j);

        protected override void SetValue(int i, int j, T value)
        {
            if (i != j)
            {
                return;
            }

            base.SetValue(i, j, value);
        }

        protected override int CalculateIndex(int i, int j) => i;
    }
}