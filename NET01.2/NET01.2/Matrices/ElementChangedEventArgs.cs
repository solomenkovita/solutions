﻿using System;

namespace NET01._2.Matrices
{
    public class ElementChangedEventArgs<T> : EventArgs
    {
        public int I { get; }
        public int J { get; }
        public T OldValue { get; }

        public ElementChangedEventArgs(int i, int j, T oldValue)
        {
            I = i;
            J = j;
            OldValue = oldValue;
        }
    }
}