﻿namespace NET01._2.Matrices
{
    public class UpperTriangularMatrix<T> : SquareMatrix<T>
    {
        public UpperTriangularMatrix(int size) : base(size, size * (size + 1) / 2)
        {
        }

        protected override T GetValue(int i, int j) => j < i ? default : base.GetValue(i, j);

        protected override void SetValue(int i, int j, T value)
        {
            if (j < i)
            {
                return;
            }

            base.SetValue(i, j, value);
        }

        protected override int CalculateIndex(int i, int j) => i * Size + j - i * (i + 1) / 2;
    }
}