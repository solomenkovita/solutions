﻿using System;
using System.Text;

namespace NET01._2.Matrices
{
    public class SquareMatrix<T>
    {
        private readonly T[] _values;

        public int Size { get; }

        public T this[int i, int j]
        {
            get
            {
                CheckIndexes(i, j);
                return GetValue(i, j);
            }
            set
            {
                CheckIndexes(i, j);
                SetValue(i, j, value);
            }
        }

        public event EventHandler<ElementChangedEventArgs<T>> ElementChanged;

        public SquareMatrix(int size) : this(size, size * size)
        {
        }

        protected SquareMatrix(int size, int numberOfElements)
        {
            if (size < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(size));
            }

            if (numberOfElements < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(numberOfElements));
            }

            Size = size;
            _values = new T[numberOfElements];
        }

        public override string ToString()
        {
            var sb = new StringBuilder();
            for (var i = 0; i < Size; i++)
            {
                for (var j = 0; j < Size; j++)
                {
                    sb.AppendFormat("{0,-10}", this[i, j]);
                }

                sb.AppendLine();
            }

            return sb.ToString();
        }

        protected virtual T GetValue(int i, int j)
        {
            var valueIndex = CalculateIndex(i, j);
            return _values[valueIndex];
        }

        protected virtual void SetValue(int i, int j, T value)
        {
            var valueIndex = CalculateIndex(i, j);
            var oldValue = _values[valueIndex];
            if (!Equals(oldValue, value))
            {
                _values[valueIndex] = value;
                OnElementChanged(new ElementChangedEventArgs<T>(i, j, oldValue));
            }
        }

        protected virtual int CalculateIndex(int i, int j) => i * Size + j;

        protected virtual void OnElementChanged(ElementChangedEventArgs<T> value)
        {
            ElementChanged?.Invoke(this, value);
        }

        private void CheckIndexes(int i, int j)
        {
            // see https://stackoverflow.com/questions/29343533/is-it-more-efficient-to-perform-a-range-check-by-casting-to-uint-instead-of-chec
            if ((uint) i >= (uint) Size)
            {
                throw new IndexOutOfRangeException(nameof(i));
            }

            if ((uint) j >= (uint) Size)
            {
                throw new IndexOutOfRangeException(nameof(j));
            }
        }
    }
}