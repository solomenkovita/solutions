﻿using System.Collections.Generic;
using SensorConfig;

namespace Factory
{
    public interface IFactory
    {
        List<Memento> LoadSensors();
        void SaveSensors(List<Memento> sensors);
    }
}