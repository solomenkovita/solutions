﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Factory;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SensorConfig;
using Utils;

namespace JsonFactory
{
    public class JsonFactory : IFactory
    {
        private readonly string _readFrom;
        private readonly string _saveTo;

        public JsonFactory(string readFrom, string saveTo)
        {
            _readFrom = readFrom;
            _saveTo = saveTo;
        }

        public List<Memento> LoadSensors()
        {
            var json = File.ReadAllText(_readFrom);
            return JsonConvert.DeserializeObject<JArray>(json).Select(x => new Memento((Guid) x["Id"],
                (int) x["Interval"],
                Helpers.ConvertSensorType((string) x["Type"]))).ToList();
        }

        public void SaveSensors(List<Memento> sensors)
        {
            if (sensors == null) return;

            var jsonSensors = JsonConvert.SerializeObject(sensors.Select(x =>
                (x.Id, x.Type, x.Interval)));

            File.WriteAllText(_saveTo, jsonSensors);
        }
    }
}