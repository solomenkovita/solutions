﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Factory;
using SensorConfig;
using SensorConfig.Modes;

namespace ViewModel
{
    public class ViewSensorModel : IViewSensorModel
    {
        private readonly SensorObserver _observer;
        private readonly ObservableCollection<Sensor> _sensors;

        private readonly IFactory j_factory =
            new JsonFactory.JsonFactory(@"Config\sensors.json", @"Config\sensorsWrite.json");

        private readonly IFactory x_factory =
            new XmlFactory.XmlFactory(@"Config\sensors.xml", @"Config\sensorsWrite.xml");

        private Sensor _currentSensor;

        public ViewSensorModel(SensorObserver observer)
        {
            _observer = observer;
            _sensors = new ObservableCollection<Sensor>();
        }

        public IEnumerable DataSource => _sensors;

        public void LoadXml()
        {
            const string filename = @"Config\sensors.xml";
            if (File.Exists(filename))
                GetSensors(x_factory.LoadSensors());
        }

        public void LoadJson()
        {
            const string filename = @"Config\sensors.json";
            if (File.Exists(filename))
                GetSensors(j_factory.LoadSensors());
        }

        public OnModelChanged SelectionChanged(object item)
        {
            _currentSensor = (Sensor) item;
            return _currentSensor == null
                ? null
                : new OnModelChanged(_currentSensor.Id, _currentSensor.Interval, _currentSensor.Type,
                    _currentSensor.Mode, _currentSensor.MeasuredValue);
        }

        public string ChangeMode()
        {
            if (_currentSensor == null) return null;
            _currentSensor.ChangeMode();
            return _currentSensor.Mode.ToString();
        }

        public void SaveToXml()
        {
            if (_sensors.Count != 0)
                x_factory.SaveSensors(GetConfigs(_sensors));
        }

        public void SaveToJson()
        {
            if (_sensors.Count != 0)
                j_factory.SaveSensors(GetConfigs(_sensors));
        }

        public void AddSensor(Guid id, int interval, SensorType type)
        {
            var memento = new Memento(id, interval, type);
            var newSensor = new Sensor(memento);
            newSensor.AddObserver(_observer);
            _sensors.Add(newSensor);
        }

        public void RemoveSensor()
        {
            if (_currentSensor == null || _sensors.Count == 0) return;
            _currentSensor.Mode.Dispose();
            _sensors.Remove(_currentSensor);
        }

        public void Dispose()
        {
            foreach (var sensor in _sensors) sensor.Mode.Dispose();
        }

        public string Stop()
        {
            if (_currentSensor == null) return null;
            _currentSensor.Mode = new SimpleMode();
            return _currentSensor.Mode.ToString();
        }

        private void GetSensors(List<Memento> list)
        {
            list.ForEach(item =>
            {
                var sensor = new Sensor(item);
                sensor.AddObserver(_observer);
                _sensors.Add(sensor);
            });
        }

        private List<Memento> GetConfigs(ObservableCollection<Sensor> list)
        {
            return list.Select(item => new Memento(item.Id, item.Interval, item.Type)).ToList();
        }
    }
}