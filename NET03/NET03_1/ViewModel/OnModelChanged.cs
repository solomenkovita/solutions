﻿using System;
using SensorConfig;
using SensorConfig.Modes;

namespace ViewModel
{
    public class OnModelChanged
    {
        public OnModelChanged(Guid id, int interval, SensorType type, Mode mode, int value)
        {
            Id = id;
            Interval = interval;
            Type = type;
            Mode = mode;
            Value = value;
        }

        public Guid Id { get; }
        public int Interval { get; }
        public SensorType Type { get; }
        public Mode Mode { get; }
        public int Value { get; }
    }
}