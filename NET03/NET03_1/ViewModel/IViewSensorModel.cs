﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SensorConfig;

namespace ViewModel
{
    public interface IViewSensorModel : IDisposable
    {
        IEnumerable DataSource { get; }
        void LoadXml();
        void LoadJson();
        OnModelChanged SelectionChanged(object item);
        string ChangeMode();
        void SaveToJson();
        void SaveToXml();
        void AddSensor(Guid id, int interval, SensorType type);
        void RemoveSensor();
        string Stop();
    }
}