﻿using System;
using System.Windows;
using System.Windows.Controls;
using SensorConfig;
using ViewModel;

namespace NET03_1
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly SensorObserver _observer;
        private readonly ViewSensorModel _viewSensorModel;

        public MainWindow()
        {
            InitializeComponent();
            _observer = new SensorObserver(SensorUpdater);
            _viewSensorModel = new ViewSensorModel(_observer);
            DataGrid.ItemsSource = _viewSensorModel.DataSource;
        }

        private void ReadFromXml_OnClick(object sender, RoutedEventArgs e)
        {
            if (DataGrid.Items.Count != 0) _viewSensorModel.Dispose();
            _viewSensorModel.LoadXml();
            SetEmpty();
        }

        private void SensorUpdater(Tuple<Guid, int> tuple)
        {
            if (tuple == null) return;

            Dispatcher?.Invoke(() =>
            {
                if (IdLabel.Content == null) return;

                if (!string.IsNullOrEmpty(IdLabel.Content.ToString()) && tuple.Item1 == (Guid) IdLabel.Content)
                    ValueTextBox.Text = tuple.Item2.ToString();
            });
        }

        private void ReadFromJson_OnClick(object sender, RoutedEventArgs e)
        {
            if (DataGrid.Items.Count != 0) _viewSensorModel.Dispose();
            _viewSensorModel.LoadJson();
            SetEmpty();
        }

        private void ChangeMode_OnClick(object sender, RoutedEventArgs e)
        {
            if (IdLabel.Content == null) return;
            var str = _viewSensorModel.ChangeMode();
            if (str == null) return;
            ModeLabel.Content = str;
        }

        private void SaveToXml_OnClick(object sender, RoutedEventArgs e)
        {
            _viewSensorModel.Dispose();
            _viewSensorModel.SaveToXml();
            MessageBox.Show("Saved");
        }

        private void SaveToJson_OnClick(object sender, RoutedEventArgs e)
        {
            _viewSensorModel.SaveToJson();
            MessageBox.Show("Saved");
        }

        private void AddSensor_OnClick(object sender, RoutedEventArgs e)
        {
            var addSensor = new AddSensorWindow(_viewSensorModel);
            addSensor.ShowDialog();
        }

        private void DataGrid_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (DataGrid.SelectedCells == null || DataGrid.SelectedCells.Count == 0) return;
            var currentSensor = _viewSensorModel.SelectionChanged(DataGrid.SelectedItem);

            if (currentSensor == null) return;
            IdLabel.Content = currentSensor.Id;
            TypeLabel.Content = currentSensor.Type;
            ModeLabel.Content = currentSensor.Mode;
            IntervalLabel.Content = currentSensor.Interval;
            ValueTextBox.Text = currentSensor.Value.ToString();
            ModeLabel.Content = currentSensor.Mode;
        }

        private void RemoveSensor_OnClick(object sender, RoutedEventArgs e)
        {
            if (IdLabel == null) return;
            _viewSensorModel.RemoveSensor();
            SetEmpty();
            DataGrid.Items.Refresh();
        }

        private void SetEmpty()
        {
            IdLabel.Content = null;
            TypeLabel.Content = null;
            ModeLabel.Content = null;
            ValueTextBox.Text = "";
            IntervalLabel.Content = null;
        }

        private void MainWindow_OnClosed(object sender, EventArgs e)
        {
            if (DataGrid.Items.Count == 0) return;
            _viewSensorModel.Dispose();
        }

        private void Stop_OnClick(object sender, RoutedEventArgs e)
        {
            if (IdLabel.Content == null) return;
            var str = _viewSensorModel.Stop();
            if (str == null) return;
            ModeLabel.Content = str;
        }
    }
}