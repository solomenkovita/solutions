﻿using System;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Input;
using Utils;
using ViewModel;

namespace NET03_1
{
    /// <summary>
    ///     Interaction logic for AddSensorWindow.xaml
    /// </summary>
    public partial class AddSensorWindow : Window
    {
        private readonly ViewSensorModel _viewSensorModel;

        public AddSensorWindow(ViewSensorModel viewSensorModel)
        {
            InitializeComponent();
            _viewSensorModel = viewSensorModel;
        }

        private void Add_OnClick(object sender, RoutedEventArgs e)
        {
            if (ComboBox.SelectedValue == null || string.IsNullOrEmpty(IntervalTextBox.Text))
            {
                MessageBox.Show("Fill all fields");
                return;
            }

            _viewSensorModel.AddSensor(Guid.Empty, Helpers.ParseInt(IntervalTextBox.Text),
                Helpers.ConvertSensorType(ComboBox.Text));
            Close();
        }

        private void IntervalTextBox_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            var regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}