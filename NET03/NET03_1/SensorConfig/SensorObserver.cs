﻿using System;

namespace SensorConfig
{
    public class SensorObserver : IObserver
    {
        private readonly Action<Tuple<Guid, int>> _func;

        public SensorObserver(Action<Tuple<Guid, int>> func)
        {
            Check.CheckValue(func);
            _func = func;
        }

        public void Update(Tuple<Guid, int> tuple)
        {
            Check.CheckValue(tuple);
            _func.Invoke(tuple);
        }
    }
}