﻿using System;

namespace SensorConfig
{
    public static class Check
    {
        public static void CheckValue(object value)
        {
            switch (value)
            {
                case null:
                    throw new ArgumentNullException(nameof(value));
                case string s when string.IsNullOrEmpty(s):
                    throw new ArgumentException(nameof(value));
            }
        }
    }
}
