﻿using System;

namespace SensorConfig
{
    public class Memento
    {
        private Guid _id;
        private int _interval;

        public Memento(Guid id, int interval, SensorType type)
        {
            Id = id;
            Interval = interval;
            Type = type;
        }

        public Guid Id
        {
            get => _id;
            private set => _id = value == Guid.Empty ? IdGenerator.GetInstance().NewGuid() : value;
        }

        public int Interval
        {
            get => _interval;
            private set
            {
                if (value <= 0) throw new ArgumentException("Interval must be positive value");
                _interval = value;
            }
        }

        public SensorType Type { get; }

        public override string ToString()
        {
            return $"Id: {Id}, Sensor type: {Type}, Interval: {Interval}";
        }
    }
}