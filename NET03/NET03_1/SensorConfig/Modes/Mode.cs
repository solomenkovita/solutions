﻿using System;
using System.Threading;

namespace SensorConfig.Modes
{
    public abstract class Mode : IDisposable
    {
        protected Timer _timer;
        public abstract void ChangeMode(Sensor sensor);
        public abstract void TakeMeasurements(Sensor sensor);

        protected void StopTimer()
        {
            if (_timer == null) return;
            _timer.Dispose();
            _timer = null;
        }

        public void Dispose()
        {
            StopTimer();
        }
    }
}