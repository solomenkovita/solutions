﻿using System;
using System.Threading;

namespace SensorConfig.Modes
{
    public class WorkingMode : Mode
    {
        public override void ChangeMode(Sensor sensor)
        {
            StopTimer();
            sensor.Mode = new SimpleMode();
        }

        public override void TakeMeasurements(Sensor sensor)
        {
            var random = new Random();
            if (_timer == null)
                _timer = new Timer(state => sensor.MeasuredValue = random.Next(), null, 0, sensor.Interval * 1000);
        }

        public override string ToString()
        {
            return "Working mode";
        }
    }
}