﻿namespace SensorConfig.Modes
{
    public class SimpleMode : Mode
    {
        public override void ChangeMode(Sensor sensor)
        {
            sensor.Mode = new CalibrationMode();
        }

        public override void TakeMeasurements(Sensor sensor) { }

        public override string ToString()
        {
            return "Simple mode";
        }
    }
}