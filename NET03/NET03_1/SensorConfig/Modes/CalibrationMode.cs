﻿using System.Threading;

namespace SensorConfig.Modes
{
    public class CalibrationMode : Mode
    {
        public override void ChangeMode(Sensor sensor)
        {
            StopTimer();
            sensor.Mode = new WorkingMode();
        }

        public override void TakeMeasurements(Sensor sensor)
        {
            if (_timer != null) return;
            sensor.MeasuredValue = 0;
            _timer = new Timer(state => sensor.MeasuredValue++, null, 0, 1000);
        }

        public override string ToString()
        {
            return "Calibration mode";
        }
    }
}