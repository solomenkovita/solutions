﻿using System;

namespace SensorConfig
{
    public interface IObserver
    {
        void Update(Tuple<Guid, int> tuple);
    }
}