﻿using System;

namespace SensorConfig
{
    public class IdGenerator
    {
        private static IdGenerator _instance = new IdGenerator();
        private static readonly object Locker = new object();

        private IdGenerator() { }

        public static IdGenerator GetInstance()
        {
            lock (Locker)
            {
                return _instance ?? (_instance = new IdGenerator());
            }
        }

        public Guid NewGuid()
        {
            return Guid.NewGuid();
        }
    }
}