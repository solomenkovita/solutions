﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using SensorConfig.Modes;

namespace SensorConfig
{
    public class Sensor : INotifyPropertyChanged
    {
        private readonly List<IObserver> _observers = new List<IObserver>();
        private int _measuredValue;
        private Mode _mode = new SimpleMode();

        public Sensor(Memento configs)
        {
            Id = configs.Id;
            Interval = configs.Interval;
            Type = configs.Type;
        }

        public Guid Id { get; }
        public int Interval { get; }
        public SensorType Type { get; }

        public int MeasuredValue
        {
            get => _measuredValue;
            set
            {
                _measuredValue = value;
                Notify();
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("MeasuredValue"));
            }
        }

        public Mode Mode
        {
            get => _mode;
            set
            {
                if (_mode != null)
                {
                    _mode.Dispose();
                    _mode = null;
                }

                _mode = value;
                if (_mode != null)
                {
                    Notify();
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Mode"));
                    _mode.TakeMeasurements(this);
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void AddObserver(IObserver observer)
        {
            Check.CheckValue(observer);
            _observers.Add(observer);
        }

        public void RemoveObserver(IObserver observer)
        {
            Check.CheckValue(observer);
            _observers.Remove(observer);
        }

        private void Notify()
        {
            foreach (var updater in _observers) updater.Update(new Tuple<Guid, int>(Id, MeasuredValue));
        }

        public void ChangeMode()
        {
            Mode.ChangeMode(this);
        }
    }
}