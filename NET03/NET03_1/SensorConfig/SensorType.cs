﻿namespace SensorConfig
{
    public enum SensorType
    {
        Temperature,
        Pressure,
        MagneticField
    }
}
