﻿using System;
using SensorConfig;

namespace Utils
{
    public static class Helpers
    {
        public static SensorType ConvertSensorType(string type)
        {
            if (string.IsNullOrEmpty(type)) throw new ArgumentNullException(nameof(type));
            return (SensorType) Enum.Parse(typeof(SensorType), type);
        }

        public static Guid ParseId(string value)
        {
            return Guid.TryParse(value, out var id) ? id : Guid.NewGuid();
        }

        public static int ParseInt(string value)
        {
            return int.TryParse(value, out var interval) ? interval : 100000;
        }
    }
}