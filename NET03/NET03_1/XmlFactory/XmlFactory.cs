﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Factory;
using SensorConfig;
using Utils;

namespace XmlFactory
{
    public class XmlFactory : IFactory
    {
        private readonly string _readFrom;
        private readonly string _saveTo;

        public XmlFactory(string readFrom, string saveTo)
        {
            _readFrom = readFrom;
            _saveTo = saveTo;
        }

        public List<Memento> LoadSensors()
        {
            var doc = XDocument.Load(_readFrom);
            var root = doc.Element("config");
            if (root == null) throw new InvalidOperationException("Root element 'config' is missing");

            return root.Elements("sensor").Select(s => new Memento(Helpers.ParseId(s.Attribute("id")?.Value),
                Helpers.ParseInt(s.Attribute("interval")?.Value),
                Helpers.ConvertSensorType(s.Attribute("type")?.Value))).ToList();
        }

        public void SaveSensors(List<Memento> sensors)
        {
            if (sensors == null) return;

            var xmlWriter = XmlWriter.Create(_saveTo);

            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("config");
            foreach (var sensor in sensors)
            {
                xmlWriter.WriteStartElement("sensor");
                xmlWriter.WriteAttributeString("id", sensor.Id.ToString());
                xmlWriter.WriteAttributeString("type", sensor.Type.ToString());
                xmlWriter.WriteAttributeString("interval", sensor.Interval.ToString());
                xmlWriter.WriteEndElement();
            }

            xmlWriter.WriteEndDocument();
            xmlWriter.Close();
        }
    }
}