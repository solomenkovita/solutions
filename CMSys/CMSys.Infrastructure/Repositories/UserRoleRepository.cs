﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using CMSys.Core.Entities;
using CMSys.Core.Repositories;

namespace CMSys.Infrastructure.Repositories
{
    internal sealed class UserRoleRepository : Repository<UserRole>, IUserRoleRepository
    {
        public UserRoleRepository(CMSysContext context) : base(context)
        {
        }

        public UserRole Find(Guid userId, Guid roleId)
        {
            return DbSet.Find(userId, roleId);
        }

        public new void Add(UserRole entity)
        {
            base.Add(entity);
        }

        public new void Remove(UserRole entity)
        {
            base.Remove(entity);
        }

        public IEnumerable<UserRole> List(Expression<Func<UserRole, bool>> predicate = null)
        {
            return null;
        }
    }
}