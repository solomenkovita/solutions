﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CMSys.Core.Common;
using CMSys.Core.Entities;
using CMSys.Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace CMSys.Infrastructure.Repositories
{
    internal sealed class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(CMSysContext context) : base(context)
        {
        }

        public IEnumerable<User> List(Expression<Func<User, bool>> predicate = null)
        {
            var query = MakeInclusions().OrderBy(x => x.LastName).ThenBy(x => x.FirstName).AsQueryable();
            if (predicate != null) query = query.Where(predicate);

            return query.ToList();
        }

        public User FindByEmail(string email)
        {
            return MakeInclusions().SingleOrDefault(x => x.Email == email);
        }

        public IEnumerable<User> ActiveUsers()
        {
            return MakeInclusions().Where(x => x.EndDate == null).OrderBy(x => x.LastName).ThenBy(x => x.FirstName)
                .AsQueryable();
        }

        public User Find(Guid id)
        {
            return MakeInclusions().SingleOrDefault(x => x.Id == id);
        }

        public async Task<(IEnumerable<User>, int)> GetPageItems(UserFilter filter, string name)
        {
            var query = MakeInclusions().OrderBy(x => x.FirstName).AsQueryable();
            if (!string.IsNullOrEmpty(name))
            {
                var tuple = GetTuple(name);
                if (!string.IsNullOrEmpty(tuple.Item2))
                    query = query.Where(x => x.LastName.Contains(tuple.Item1) || x.LastName.Contains(tuple.Item2))
                        .Where(x => x.FirstName.Contains(tuple.Item1) || x.FirstName.Contains(tuple.Item2));
                else
                    query = query.Where(x => x.LastName.Contains(tuple.Item1) || x.FirstName.Contains(tuple.Item1));
            }

            var count = await query.CountAsync();
            query = query.Skip(filter.Start).Take(filter.Size);

            var items = await query.ToListAsync();
            return (items, count);
        }

        private IQueryable<User> MakeInclusions()
        {
            return DbSet.Include(x => x.UserRole).ThenInclude(x => x.Role);
        }

        private (string, string) GetTuple(string str)
        {
            var words = str.Split(' ');
            return words.Length == 1 ? (words[0], "") : (words[0], words[1]);
        }
    }
}