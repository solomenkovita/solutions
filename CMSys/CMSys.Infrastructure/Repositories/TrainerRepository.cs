﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using CMSys.Core.Entities;
using CMSys.Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace CMSys.Infrastructure.Repositories
{
    internal sealed class TrainerRepository : Repository<Trainer>, ITrainerRepository
    {
        public TrainerRepository(CMSysContext context) : base(context)
        {
        }

        public IEnumerable<Trainer> List(Expression<Func<Trainer, bool>> predicate = null)
        {
            var query = MakeInclusions().OrderBy(x => x.VisualOrder).AsQueryable();
            if (predicate != null) query = query.Where(predicate);

            return query.ToList();
        }

        public IEnumerable<IGrouping<string, Trainer>> Groups()
        {
            return MakeInclusions().ToList().OrderBy(x => x.VisualOrder).GroupBy(x => x.User.Department);
        }

        public int GetCount()
        {
            return DbSet.Count();
        }

        public Trainer Find(Guid id)
        {
            return MakeInclusions().SingleOrDefault(x => x.Id == id);
        }

        private IQueryable<Trainer> MakeInclusions()
        {
            return DbSet.Include(x => x.User).Include(x => x.TrainerGroup);
        }
    }
}