﻿using CMSys.Core.Entities;
using Microsoft.EntityFrameworkCore;

namespace CMSys.Infrastructure.Repositories
{
    internal abstract class Repository<TEntity> where TEntity : Entity
    {
        protected readonly CMSysContext Context;
        protected readonly DbSet<TEntity> DbSet;

        protected Repository(CMSysContext context)
        {
            Context = context;
            DbSet = Context.Set<TEntity>();
        }

        public void Add(TEntity entity)
        {
            DbSet.Add(entity);
        }

        public void Remove(TEntity entity)
        {
            DbSet.Remove(entity);
        }
    }
}