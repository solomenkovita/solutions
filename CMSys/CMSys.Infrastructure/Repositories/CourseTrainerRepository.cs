﻿using System;
using CMSys.Core.Entities;
using CMSys.Core.Repositories;

namespace CMSys.Infrastructure.Repositories
{
    internal sealed class CourseTrainerRepository : Repository<CourseTrainer>, ICourseTrainerRepository
    {
        public CourseTrainerRepository(CMSysContext context) : base(context)
        {
        }

        public CourseTrainer Find(Guid trainerId, Guid courseId)
        {
            return DbSet.Find(courseId, trainerId);
        }
    }
}
