﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using CMSys.Core.Entities;
using CMSys.Core.Repositories;

namespace CMSys.Infrastructure.Repositories
{
    internal sealed class CourseGroupRepository : Repository<CourseGroup>, ICourseGroupRepository
    {
        public CourseGroupRepository(CMSysContext context) : base(context)
        {
        }

        public IEnumerable<CourseGroup> List(Expression<Func<CourseGroup, bool>> predicate = null)
        {
            var query = DbSet.OrderBy(x => x.VisualOrder).ToList();
            return query;
        }

        public CourseGroup Find(Guid id)
        {
            return DbSet.SingleOrDefault(x => x.Id == id);
        }
    }
}