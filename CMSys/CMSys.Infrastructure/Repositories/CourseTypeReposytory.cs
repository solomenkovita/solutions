﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using CMSys.Core.Entities;
using CMSys.Core.Repositories;

namespace CMSys.Infrastructure.Repositories
{
    internal sealed class CourseTypeRepository : Repository<CourseType>, ICourseTypeRepository
    {
        public CourseTypeRepository(CMSysContext context) : base(context)
        {
        }

        public IEnumerable<CourseType> List(Expression<Func<CourseType, bool>> predicate = null)
        {
            var query = DbSet.OrderBy(x => x.VisualOrder).ToList();
            return query;
        }

        public CourseType Find(Guid id)
        {
            return null;
        }
    }
}