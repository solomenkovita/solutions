﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using CMSys.Core.Entities;
using CMSys.Core.Repositories;

namespace CMSys.Infrastructure.Repositories
{
    internal sealed class TrainerGroupRepository : Repository<TrainerGroup>, ITrainerGroupRepository
    {
        public TrainerGroupRepository(CMSysContext context) : base(context)
        {
        }

        public IEnumerable<TrainerGroup> List(Expression<Func<TrainerGroup, bool>> predicate = null)
        {
            var query = DbSet.OrderBy(x => x.VisualOrder).ToList();
            return query;
        }

        public TrainerGroup Find(Guid id)
        {
            return DbSet.SingleOrDefault(x => x.Id == id);
        }
    }
}