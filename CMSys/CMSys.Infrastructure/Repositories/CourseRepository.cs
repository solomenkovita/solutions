﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CMSys.Core.Common;
using CMSys.Core.Entities;
using CMSys.Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace CMSys.Infrastructure.Repositories
{
    internal sealed class CourseRepository : Repository<Course>, ICourseRepository
    {
        public CourseRepository(CMSysContext context) : base(context)
        {
        }

        public IEnumerable<Course> List(Expression<Func<Course, bool>> predicate = null)
        {
            var query = MakeInclusions().OrderBy(x => x.VisualOrder).AsQueryable();
            if (predicate != null) query = query.Where(predicate);

            return query.ToList();
        }

        public async Task<(IEnumerable<Course>, int)> GetPageItems(CourseFilter courseFilter)
        {
            var query = MakeInclusions().OrderBy(x => x.CourseGroup.VisualOrder).ThenBy(x => x.VisualOrder)
                .AsQueryable();
            if (courseFilter.Filter != null) query = query.Where(courseFilter.Filter);

            var count = await query.CountAsync();
            query = query.Skip(courseFilter.Start).Take(courseFilter.Size);

            var items = await query.ToListAsync();
            foreach (var course in items)
                course.CourseTrainers = course.CourseTrainers.OrderBy(t => t.VisualOrder).ToList();
            return (items, count);
        }

        public Course Find(Guid id)
        {
            var item = MakeInclusions().SingleOrDefault(x => x.Id == id);
            if (item != null) item.CourseTrainers = item.CourseTrainers.OrderBy(t => t.VisualOrder).ToList();
            return item;
        }

        private IQueryable<Course> MakeInclusions()
        {
            return DbSet.Include(x => x.CourseTrainers).ThenInclude(x => x.Trainer).ThenInclude(x => x.User)
                .Include(x => x.CourseGroup).Include(x => x.CourseType);
        }
    }
}