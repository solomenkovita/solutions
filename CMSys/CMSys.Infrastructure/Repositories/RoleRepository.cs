﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using CMSys.Core.Entities;
using CMSys.Core.Repositories;
using Microsoft.EntityFrameworkCore;

namespace CMSys.Infrastructure.Repositories
{
    internal sealed class RoleRepository : Repository<Role>, IRoleRepository
    {
        public RoleRepository(CMSysContext context) : base(context)
        {
        }

        public IEnumerable<Role> List(Expression<Func<Role, bool>> predicate = null)
        {
            var query = MakeInclusions().OrderBy(x => x.Name).AsQueryable();
            if (predicate != null) query = query.Where(predicate);

            return query.ToList();
        }

        public Role Find(Guid id)
        {
            return MakeInclusions().SingleOrDefault(x => x.Id == id);
        }

        private IQueryable<Role> MakeInclusions()
        {
            return DbSet.Include(x => x.UserRole).ThenInclude(x => x.User);
        }
    }
}