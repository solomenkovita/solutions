﻿using System;
using CMSys.Core.Repositories;
using CMSys.Infrastructure.Repositories;

namespace CMSys.Infrastructure
{
    public sealed class UnitOfWork : IUnitOfWork
    {
        private readonly CMSysContext _context;

        private ICourseGroupRepository _courseGroupRepository;
        private ICourseRepository _courseRepository;
        private ICourseTrainerRepository _courseTrainerRepository;
        private ICourseTypeRepository _courseTypeRepository;

        private bool _isDisposed;
        private IRoleRepository _roleRepository;
        private ITrainerGroupRepository _trainerGroupRepository;
        private ITrainerRepository _trainerRepository;
        private IUserRepository _userRepository;
        private IUserRoleRepository _userRoleRepository;

        public UnitOfWork(CMSysContext cMSysContext)
        {
            _context = cMSysContext ?? throw new ArgumentNullException(nameof(cMSysContext));
        }

        public IUserRepository UserRepository => _userRepository ??= new UserRepository(_context);

        public ICourseGroupRepository CourseGroupRepository =>
            _courseGroupRepository ??= new CourseGroupRepository(_context);

        public ICourseTrainerRepository CourseTrainerRepository =>
            _courseTrainerRepository ??= new CourseTrainerRepository(_context);

        public ICourseRepository CourseRepository => _courseRepository ??= new CourseRepository(_context);

        public ICourseTypeRepository CourseTypeRepository =>
            _courseTypeRepository ??= new CourseTypeRepository(_context);

        public IRoleRepository RoleRepository => _roleRepository ??= new RoleRepository(_context);

        public ITrainerRepository TrainerRepository => _trainerRepository ??= new TrainerRepository(_context);

        public ITrainerGroupRepository TrainerGroupRepository =>
            _trainerGroupRepository ??= new TrainerGroupRepository(_context);

        public IUserRoleRepository UserRoleRepository => _userRoleRepository ??= new UserRoleRepository(_context);

        public void Commit()
        {
            if (_isDisposed) throw new ObjectDisposedException("UnitOfWork");

            _context.SaveChanges();
        }

        public void Dispose()
        {
            if (_context == null) return;

            if (!_isDisposed) _context.Dispose();

            _isDisposed = true;
        }
    }
}