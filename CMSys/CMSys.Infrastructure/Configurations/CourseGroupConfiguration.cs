﻿using CMSys.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CMSys.Infrastructure.Configurations
{
    internal sealed class CourseGroupConfiguration : IEntityTypeConfiguration<CourseGroup>
    {
        public void Configure(EntityTypeBuilder<CourseGroup> builder)
        {
            builder.ToTable(nameof(CourseGroup), "Catalog");

            builder.HasKey(x => x.Id);

            builder.HasIndex(x => x.Name).HasName("UK_Catalog_CourseGroup_Name").IsUnique();
            builder.Property(x => x.Description).HasMaxLength(256);
            builder.Property(x => x.VisualOrder).HasColumnType("int");
        }
    }
}
