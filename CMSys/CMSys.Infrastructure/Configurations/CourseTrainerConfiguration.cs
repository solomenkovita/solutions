﻿using CMSys.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CMSys.Infrastructure.Configurations
{
    internal sealed class CourseTrainerConfiguration : IEntityTypeConfiguration<CourseTrainer>
    {
        public void Configure(EntityTypeBuilder<CourseTrainer> builder)
        {
            builder.ToTable(nameof(CourseTrainer), "Catalog");

            builder.HasKey(x => new { x.CourseId, x.TrainerId }).HasName("PK_Catalog_CourseTrainer");

            builder.HasOne(x => x.Trainer).WithMany(x => x.CourseTrainers).HasForeignKey(x => x.TrainerId)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(x => x.Course).WithMany(x => x.CourseTrainers).HasForeignKey(x => x.CourseId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
