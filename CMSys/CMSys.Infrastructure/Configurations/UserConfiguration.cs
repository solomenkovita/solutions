﻿using CMSys.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CMSys.Infrastructure.Configurations
{
    internal sealed class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable(nameof(User), "Membership");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.FirstName).IsRequired().HasMaxLength(128);
            builder.Property(x => x.LastName).IsRequired().HasMaxLength(128);
            builder.HasIndex(x => x.Email).HasName("UK_Membership_User_Email").IsUnique();
            builder.Property(x => x.PasswordHash).IsRequired().HasMaxLength(128);
            builder.Property(x => x.PasswordSalt).IsRequired().HasMaxLength(128);
            builder.Property(x => x.StartDate).HasColumnType("date").IsRequired();
            builder.Property(x => x.EndDate).HasColumnType("date");
            builder.Property(x => x.Department).HasMaxLength(128);
            builder.Property(x => x.Position).HasMaxLength(128);
            builder.Property(x => x.Location).HasMaxLength(128);
            builder.Property(x => x.Photo);
        }
    }
}
