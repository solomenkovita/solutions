﻿using CMSys.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CMSys.Infrastructure.Configurations
{
    internal sealed class CourseConfiguration : IEntityTypeConfiguration<Course>
    {
        public void Configure(EntityTypeBuilder<Course> builder)
        {
            builder.ToTable(nameof(Course), "Catalog");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Name).IsRequired().HasMaxLength(64);
            builder.Property(x => x.Description).HasMaxLength(4000);
            builder.Property(x => x.VisualOrder).HasColumnType("int");
            builder.Property(x => x.IsNew).HasColumnType("bit");

            builder.HasOne(x => x.CourseGroup).WithMany(x => x.Course).HasForeignKey(x => x.CourseGroupId)
                .OnDelete(DeleteBehavior.Cascade).HasConstraintName("FK_Catalog_Course_Catalog_CourseGroup");

            builder.HasOne(x => x.CourseType).WithMany(x => x.Course).HasForeignKey(x => x.CourseTypeId)
                .OnDelete(DeleteBehavior.Cascade).HasConstraintName("FK_Catalog_Course_Catalog_CourseType");
        }
    }
}
