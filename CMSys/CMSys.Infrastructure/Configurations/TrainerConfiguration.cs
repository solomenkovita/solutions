﻿using CMSys.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CMSys.Infrastructure.Configurations
{
    internal sealed class TrainerConfiguration : IEntityTypeConfiguration<Trainer>
    {
        public void Configure(EntityTypeBuilder<Trainer> builder)
        {
            builder.ToTable(nameof(Trainer), "Catalog");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Description).HasMaxLength(4000);
            builder.Property(x => x.VisualOrder).HasColumnType("int");

            builder.HasOne(x => x.User).WithOne().HasForeignKey<Trainer>(x => x.Id)
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(x => x.TrainerGroup).WithMany(x => x.Trainer).HasForeignKey(x => x.TrainerGroupId)
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
