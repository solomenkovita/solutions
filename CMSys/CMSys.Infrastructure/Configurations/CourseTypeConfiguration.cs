﻿using CMSys.Core.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CMSys.Infrastructure.Configurations
{
    internal sealed class CourseTypeConfiguration : IEntityTypeConfiguration<CourseType>
    {
        public void Configure(EntityTypeBuilder<CourseType> builder)
        {
            builder.ToTable(nameof(CourseType), "Catalog");

            builder.HasKey(x => x.Id);

            builder.HasIndex(x => x.Name).HasName("UK_Catalog_CourseType_Name").IsUnique();
            builder.Property(x => x.Description).HasMaxLength(256);
            builder.Property(x => x.VisualOrder).HasColumnType("int");
        }
    }
}
