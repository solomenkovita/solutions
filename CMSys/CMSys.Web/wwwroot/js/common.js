﻿function sendAjaxRequestTrainer(httpMethod, url) {
    $.ajax("/api/web" + (url ? "/" + url : ""),
        {
            type: httpMethod,
            success: function (data) {
                $(".namediv").text(data.firstName + " " + data.lastName);
                $(".descdiv").text(data.description);
                $(".detailsimg").attr('src', `data:image/png;base64,${data.photo}`);
                $('#modDialog').modal('show');
            }
        });
}

function sendAjaxRequestGroup(httpMethod, url, controller) {
    $.ajax("/api/web" + (url ? "/" + url : ""),
        {
            type: httpMethod,
            success: function (data) {
                $(".headerdiv").text(data.name);
                $(".idlabel").attr("value", data.id);
                $(".namelabel")[0].value = data.name;
                $(".descriptionlabel").text(data.description);
                $(".orderlabel")[0].value = data.visualOrder;
                $(".submitinput").attr("value", "Edit");
                $(".formpopup").attr("action", `/Admin/${controller}/Edit`);
                $('.namespan').html("");
                $('.orderspan').html("");
                $('#modDialog').modal('show');
            }
        });
}

function getTrainer(id) {
    sendAjaxRequestTrainer("GET", "Trainer/" + id);
}

function getEditForGroup(id, controller) {
    sendAjaxRequestGroup("GET", `${controller}/` + id, controller);
}

function AddGroup(controller) {
    $(".namelabel")[0].value = "";
    $(".descriptionlabel").text("");
    $(".orderlabel")[0].value = "";
    $(".submitinput").attr("value", "Add");
    $(".formpopup").attr("action", `/Admin/${controller}/Add`);
    $('.namespan').html("");
    $('.orderspan').html("");
    $('#modDialog3').modal('show');
}

function Delete(controller, id) {
    $(".dodelete").attr("action", `/Admin/${controller}/Delete/${id}`);
    $('#modDialog2').modal('show');

}

function DeleteGroup(id) {
    $(".dodelete").attr("action", `/Admin/CourseGroup/Delete/${id}`);
    $('#modDialog2').modal('show');

}

function onTrainerClick(event) {
    getTrainer(event.srcElement.id);
}

function onEditGroupClick(event) {
    getEditForGroup(event.srcElement.id, "CourseGroup");
}

function onEditTrainerGroupClick(event) {
    getEditForGroup(event.srcElement.id, "TrainerGroup");
}

function onAddGroupClick(event) {
    AddGroup("CourseGroup");
}

function onAddTrainerGroupClick(event) {
    AddGroup("TrainerGroup");
}

function onDeleteGroupClick(event) {
    Delete("CourseGroup", event.srcElement.id);
}

function onDeleteCourseClick(event) {
    Delete("Course", event.srcElement.id);
}

function onDeleteTrainerClick(event) {
    Delete("Trainer", event.srcElement.id);
}

function onButtonEditClick(event) {
    var id = $(".idlabel").val();
    console.log(id);
    var name = $(".namelabel").val();
    var order = $(".orderlabel").val();
    var description = $(".descriptionlabel").val();
    if (name == "")
        $(".namespan").text("Name is required");
    else {
        if (order == "")
            $(".orderspan").text("Order is required");
        else
            sendAjaxRequestEdit("GET", `EditCourseGroup/${id}/${name}|${description}|${order}`);
    }
}

function sendAjaxRequestEdit(httpMethod, url) {
    console.log(url);
    $.ajax("/api/web" + (url ? "/" + url : ""),
        {
            type: httpMethod,
            success: function (data) {
                $(`#${data.id}`).children(".group_name").text(data.name);
                $(`#${data.id}`).children(".group_description").text(data.description);
                $(`#${data.id}`).children(".group_order").text(data.order);
                $('#modDialog').modal('hide');
            }
        });
}

function onDeleteTrainerGroupClick(event) {
    Delete("TrainerGroup", event.srcElement.id);
}