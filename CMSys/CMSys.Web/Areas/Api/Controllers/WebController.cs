﻿using System;
using System.Web.Http;
using CMSys.Core.Entities;
using CMSys.Core.Repositories;
using CMSys.Web.Areas.Admin.ViewModels;
using CMSys.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Areas.Api.Controllers
{
    [Microsoft.AspNetCore.Mvc.Route("api/[controller]")]
    public class WebController : ApiController
    {
        private readonly IUnitOfWork _uow;

        public WebController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("Trainer/{id}")]
        public TrainerViewModel GetTrainer([FromRoute] Guid id)
        {
            return Build(_uow.TrainerRepository.Find(id));
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("CourseGroup/{id}")]
        public EditGroupViewModel GetCourseGroup([FromRoute] Guid id)
        {
            return Build(_uow.CourseGroupRepository.Find(id));
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("TrainerGroup/{id}")]
        public EditGroupViewModel GetTrainerGroup([FromRoute] Guid id)
        {
            return Build(_uow.TrainerGroupRepository.Find(id));
        }

        [Microsoft.AspNetCore.Mvc.HttpGet("EditCourseGroup/{id}/{data}")]
        public EditGroupViewModel EditCourseGroup([FromRoute] Guid id, [FromRoute] string data)
        {
            var group = _uow.CourseGroupRepository.Find(id);
            var parameters = data.Split("|");
            var name = parameters.Length > 0 ? parameters[0] : "-";
            var description = parameters.Length > 1 ? parameters[1] : "-";
            var order = parameters.Length > 2 ? Convert.ToInt32(parameters[2]) : 0;
            if (group == null && !ModelState.IsValid) return BuildFields(id, name, description, order);

            group.Name = name;
            group.Description = description;
            group.VisualOrder = order;

            _uow.Commit();
            return BuildFields(id, name, description, order);
        }

        private static EditGroupViewModel Build(CourseGroup courseGroup)
        {
            var model = new EditGroupViewModel
            {
                Id = courseGroup.Id,
                Name = courseGroup.Name,
                Description = courseGroup.Description,
                VisualOrder = courseGroup.VisualOrder
            };

            return model;
        }

        private static EditGroupViewModel BuildFields(Guid id, string name, string description, int order)
        {
            var model = new EditGroupViewModel
            {
                Id = id,
                Name = name,
                Description = description,
                VisualOrder = order
            };

            return model;
        }

        private static EditGroupViewModel Build(TrainerGroup trainerGroup)
        {
            var model = new EditGroupViewModel
            {
                Id = trainerGroup.Id,
                Name = trainerGroup.Name,
                Description = trainerGroup.Description,
                VisualOrder = trainerGroup.VisualOrder
            };

            return model;
        }

        private static TrainerViewModel Build(Trainer trainer)
        {
            var model = new TrainerViewModel
            {
                Id = trainer.Id,
                LastName = trainer.User.LastName,
                FirstName = trainer.User.FirstName,
                Description = trainer.Description,
                Photo = trainer.User.Photo
            };

            return model;
        }
    }
}