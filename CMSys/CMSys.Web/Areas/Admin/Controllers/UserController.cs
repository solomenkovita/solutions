﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMSys.Core.Common;
using CMSys.Core.Entities;
using CMSys.Core.Repositories;
using CMSys.Web.Areas.Admin.ViewModels;
using CMSys.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("admin/[controller]")]
    public class UserController : Controller
    {
        private readonly IUnitOfWork _uow;

        public UserController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [Route("")]
        [Route("[area]/[controller]/[action]")]
        public async Task<IActionResult> List(int page = 1, UserViewModel model = null)
        {
            var userFilter = new UserFilter(page);
            var tuple = await _uow.UserRepository.GetPageItems(userFilter, model?.Name);
            userFilter.Count = tuple.Item2;
            return View(new UserViewModel
            {
                UserFilter = userFilter,
                PageModel = PageViewModel<User>.Create(tuple.Item1, tuple.Item2, page, userFilter.Size, default,
                    default)
            });
        }

        [Route("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult Details(Guid id, int page)
        {
            var user = _uow.UserRepository.Find(id);
            return user == null
                ? (IActionResult) NotFound()
                : View(new EditUserViewModel
                {
                    User = user,
                    Page = page
                });
        }

        [HttpGet("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult Edit(Guid id, int page)
        {
            var user = _uow.UserRepository.Find(id);
            if (user == null)
                return NotFound();

            return View(new EditUserViewModel
            {
                Page = page,
                User = user,
                Roles = GetExtraRoles(user)
            });
        }

        [HttpPost("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult Edit(EditUserViewModel model)
        {
            var user = _uow.UserRepository.Find(model.User.Id);
            if (user == null) return NotFound();
            if (!ModelState.IsValid)
            {
                model.User = user;
                return View(model);
            }

            user.PasswordSalt = Guid.NewGuid().ToString();
            user.PasswordHash = UserPasswords.ComputeHash(model.NewPassword, user.PasswordSalt);

            _uow.Commit();
            return RedirectToAction("Edit", new {id = user.Id, page = model.Page});
        }

        [HttpPost("[area]/[controller]/[action]")]
        public IActionResult AddRole(EditUserViewModel model)
        {
            var user = _uow.UserRepository.Find(model.User.Id);
            if (user == null) return NotFound();
            var role = _uow.RoleRepository.Find(model.Role);

            var userRole = new UserRole {Role = role, RoleId = role.Id, User = user, UserId = user.Id};
            _uow.UserRoleRepository.Add(userRole);

            _uow.Commit();
            return RedirectToAction("Edit", new {id = user.Id, page = model.Page});
        }

        [HttpPost("[area]/[controller]/[action]")]
        public IActionResult RemoveRole(EditUserViewModel model)
        {
            var user = _uow.UserRepository.Find(model.User.Id);
            if (user == null) return NotFound();
            var userRole = _uow.UserRoleRepository.Find(user.Id, model.Role);
            if (userRole == null) return RedirectToAction("Edit", new {id = user.Id, page = model.Page});
            _uow.UserRoleRepository.Remove(userRole);
            _uow.Commit();
            return RedirectToAction("Edit", new {id = user.Id, page = model.Page});
        }

        public IEnumerable<Role> GetExtraRoles(User user)
        {
            var roles = new List<Role>();
            roles.AddRange(_uow.RoleRepository.List());
            var count = user.UserRole.Count();
            for (var i = 0; i < count; i++) roles.Remove(user.UserRole.ElementAt(i).Role);

            return roles;
        }
    }
}