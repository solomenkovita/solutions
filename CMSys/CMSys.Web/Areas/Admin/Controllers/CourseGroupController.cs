﻿using System;
using CMSys.Core.Entities;
using CMSys.Core.Repositories;
using CMSys.Web.Areas.Admin.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("admin/[controller]")]
    public class CourseGroupController : Controller
    {
        private readonly IUnitOfWork _uow;

        public CourseGroupController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [Route("")]
        [Route("[action]")]
        public IActionResult List()
        {
            return View(new CourseGroupViewModel
            {
                Groups = _uow.CourseGroupRepository.List(),
                CourseGroup = new CourseGroup()
            });
        }

        [HttpPost("[action]")]
        public IActionResult Edit(CourseGroupViewModel model)
        {
            var group = _uow.CourseGroupRepository.Find(model.CourseGroup.Id);
            if (group == null) return NotFound();

            group.Name = model.CourseGroup.Name;
            group.Description = model.CourseGroup.Description;
            group.VisualOrder = model.CourseGroup.VisualOrder;

            _uow.Commit();
            return RedirectToAction("List");
        }

        [HttpPost("[action]/{id:guid}")]
        public IActionResult Delete(Guid id)
        {
            var group = _uow.CourseGroupRepository.Find(id);
            if (group == null)
                return NotFound();
            if (group.Course == null)
            {
                _uow.CourseGroupRepository.Remove(group);
                _uow.Commit();
            }

            return RedirectToAction("List");
        }

        [HttpPost("[action]")]
        public IActionResult Add(CourseGroupViewModel model)
        {
            if (!ModelState.IsValid) return NotFound();

            var group = new CourseGroup();

            group.Id = Guid.NewGuid();
            group.Name = model.CourseGroup.Name;
            group.Description = model.CourseGroup.Description;
            group.VisualOrder = model.CourseGroup.VisualOrder;

            _uow.CourseGroupRepository.Add(group);
            _uow.Commit();
            return RedirectToAction("List");
        }
    }
}