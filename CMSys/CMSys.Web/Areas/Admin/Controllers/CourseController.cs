﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CMSys.Core.Common;
using CMSys.Core.Entities;
using CMSys.Core.Repositories;
using CMSys.Web.Areas.Admin.ViewModels;
using CMSys.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("admin/[controller]")]
    public class CourseController : Controller
    {
        private readonly IUnitOfWork _uow;

        public CourseController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [Route("")]
        [Route("[area]/[controller]/[action]")]
        public async Task<IActionResult> List(int page = 1, Guid groupId = default, Guid typeId = default)
        {
            var courseFilter = new CourseFilter(groupId, typeId, page);
            var tuple = await _uow.CourseRepository.GetPageItems(courseFilter);
            return View(new CourseViewModel
            {
                PageModel = PageViewModel<Course>.Create(tuple.Item1, tuple.Item2, page, courseFilter.Size, groupId,
                    typeId),
                Groups = _uow.CourseGroupRepository.List(),
                Types = _uow.CourseTypeRepository.List(),
                CurrentType = typeId,
                CurrentGroup = groupId
            });
        }

        [Route("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult Details(Guid id)
        {
            var course = _uow.CourseRepository.Find(id);
            return course == null ? (IActionResult) NotFound() : View(course);
        }

        [HttpGet("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult Edit(Guid id)
        {
            var course = _uow.CourseRepository.Find(id);
            if (course == null)
                return NotFound();

            return View(new EditCourseViewModel
            {
                Course = course,
                Groups = _uow.CourseGroupRepository.List(),
                Types = _uow.CourseTypeRepository.List()
            });
        }

        [HttpPost("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult Edit(EditCourseViewModel model)
        {
            if (!ModelState.IsValid)
            {
                model.Groups = _uow.CourseGroupRepository.List();
                model.Types = _uow.CourseTypeRepository.List();
                return View(model);
            }

            var course = _uow.CourseRepository.Find(model.Course.Id);
            if (course == null) return NotFound();

            var group = new CourseGroup();
            if (model.Course.CourseGroupId != null) group = GetGroupById(model.Course.CourseGroupId.Value);

            var type = new CourseType();
            if (model.Course.CourseTypeId != null) type = GetTypeById(model.Course.CourseTypeId.Value);

            course.CourseType = type;
            course.CourseTypeId = type.Id;
            course.CourseGroup = group;
            course.CourseGroupId = group.Id;
            course.Id = model.Course.Id;
            course.Name = model.Course.Name;
            course.VisualOrder = model.Course.VisualOrder;
            course.CourseTrainers = model.Course.CourseTrainers;
            course.Description = model.Course.Description;
            course.IsNew = model.Course.IsNew;

            _uow.Commit();
            return RedirectToAction("List");
        }

        [HttpGet("[area]/[controller]/[action]")]
        public IActionResult Add()
        {
            return View(new EditCourseViewModel
            {
                Groups = _uow.CourseGroupRepository.List(),
                Types = _uow.CourseTypeRepository.List()
            });
        }

        [HttpPost("[area]/[controller]/[action]")]
        public IActionResult Add(EditCourseViewModel model)
        {
            if (!ModelState.IsValid)
            {
                model.Groups = _uow.CourseGroupRepository.List();
                model.Types = _uow.CourseTypeRepository.List();
                return View(model);
            }

            var course = new Course();
            var group = new CourseGroup();
            if (model.Course.CourseGroupId != null) group = GetGroupById(model.Course.CourseGroupId.Value);

            var type = new CourseType();
            if (model.Course.CourseTypeId != null) type = GetTypeById(model.Course.CourseTypeId.Value);

            course.Id = Guid.NewGuid();
            course.CourseType = type;
            course.CourseTypeId = type.Id;
            course.CourseGroup = group;
            course.CourseGroupId = group.Id;
            course.Name = model.Course.Name;
            course.VisualOrder = model.Course.VisualOrder;
            course.Description = model.Course.Description;
            course.IsNew = model.Course.IsNew;

            _uow.CourseRepository.Add(course);
            _uow.Commit();
            return RedirectToAction("List");
        }

        public CourseGroup GetGroupById(Guid id)
        {
            return _uow.CourseGroupRepository.List().FirstOrDefault(gr => gr.Id.CompareTo(id) == 0);
        }

        public CourseType GetTypeById(Guid id)
        {
            return _uow.CourseTypeRepository.List().FirstOrDefault(t => t.Id.CompareTo(id) == 0);
        }

        [HttpPost("[action]/{id:guid}")]
        public IActionResult Delete(Guid id)
        {
            var course = _uow.CourseRepository.Find(id);
            if (course == null)
                return NotFound();

            _uow.CourseRepository.Remove(course);
            _uow.Commit();

            return RedirectToAction("List");
        }

        public IEnumerable<Trainer> GetExtraTrainers(Course course)
        {
            var trainers = new List<Trainer>();
            trainers.AddRange(_uow.TrainerRepository.List().ToList());
            var count = course.CourseTrainers.Count();
            for (var i = 0; i < count; i++) trainers.Remove(course.CourseTrainers.ElementAt(i).Trainer);

            return trainers;
        }

        [HttpGet("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult EditTrainers(Guid id)
        {
            var course = _uow.CourseRepository.Find(id);
            if (course == null)
                return NotFound();

            return View(new EditCourseTrainersViewModel
            {
                TrainerId = Guid.Empty,
                Course = course,
                Trainers = GetExtraTrainers(course)
            });
        }

        [HttpPost("[area]/[controller]/[action]")]
        public IActionResult RemoveTrainer(Guid courseId, Guid trainerId)
        {
            var course = _uow.CourseRepository.Find(courseId);
            if (course == null) return NotFound();

            var trainer = course.CourseTrainers.SingleOrDefault(x => x.TrainerId == trainerId);

            course.CourseTrainers.Remove(trainer);
            _uow.Commit();

            return RedirectToAction("EditTrainers", "Course", new {id = courseId});
        }

        [HttpPost("[area]/[controller]/[action]")]
        public IActionResult AddTrainer(Guid courseId, Guid trainerId)
        {
            var course = _uow.CourseRepository.Find(courseId);
            if (course == null) return NotFound();

            var trainer = _uow.TrainerRepository.Find(trainerId);
            int order;
            if (course.CourseTrainers.Count == 0)
                order = 10;
            else
                order = course.CourseTrainers.Last().VisualOrder + 10;
            var courseTrainer = new CourseTrainer
            {
                Course = course,
                CourseId = course.Id,
                Trainer = trainer,
                TrainerId = trainer.Id,
                VisualOrder = order
            };

            course.CourseTrainers.Add(courseTrainer);
            _uow.Commit();

            return RedirectToAction("EditTrainers", "Course", new {id = courseId});
        }
    }
}