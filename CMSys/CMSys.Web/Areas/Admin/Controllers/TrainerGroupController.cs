﻿using System;
using CMSys.Core.Entities;
using CMSys.Core.Repositories;
using CMSys.Web.Areas.Admin.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("admin/[controller]")]
    public class TrainerGroupController : Controller
    {
        private readonly IUnitOfWork _uow;

        public TrainerGroupController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [Route("")]
        [Route("[area]/[controller]/[action]")]
        public IActionResult List()
        {
            return View(new TrainerGroupViewModel
            {
                Groups = _uow.TrainerGroupRepository.List(),
                TrainerGroup = new TrainerGroup()
            });
        }

        [HttpPost("[action]")]
        public IActionResult Edit(TrainerGroupViewModel model)
        {
            var group = _uow.TrainerGroupRepository.Find(model.TrainerGroup.Id);
            if (group == null) return NotFound();

            group.Name = model.TrainerGroup.Name;
            group.Description = model.TrainerGroup.Description;
            group.VisualOrder = model.TrainerGroup.VisualOrder;

            _uow.Commit();
            return RedirectToAction("List");
        }

        [HttpPost("[action]/{id:guid}")]
        public IActionResult Delete(Guid id)
        {
            var group = _uow.TrainerGroupRepository.Find(id);
            if (group == null)
                return NotFound();
            if (group.Trainer == null)
            {
                _uow.TrainerGroupRepository.Remove(group);
                _uow.Commit();
            }

            return RedirectToAction("List");
        }

        [HttpPost("[action]")]
        public IActionResult Add(TrainerGroupViewModel model)
        {
            var group = new TrainerGroup();

            group.Id = Guid.NewGuid();
            group.Name = model.TrainerGroup.Name;
            group.Description = model.TrainerGroup.Description;
            group.VisualOrder = model.TrainerGroup.VisualOrder;

            _uow.TrainerGroupRepository.Add(group);
            _uow.Commit();
            return RedirectToAction("List");
        }
    }
}