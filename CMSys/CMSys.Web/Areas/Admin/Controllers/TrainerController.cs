﻿using System;
using System.Collections.Generic;
using System.Linq;
using CMSys.Core.Entities;
using CMSys.Core.Repositories;
using CMSys.Web.Areas.Admin.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    [Route("admin/[controller]")]
    public class TrainerController : Controller
    {
        private readonly IUnitOfWork _uow;

        public TrainerController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [Route("")]
        [Route("[area]/[controller]/[action]")]
        public IActionResult List()
        {
            return View(_uow.TrainerRepository.List());
        }

        [HttpGet("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult Edit(Guid id)
        {
            var trainer = _uow.TrainerRepository.Find(id);
            if (trainer == null)
                return NotFound();

            return View(new EditTrainerViewModel
            {
                Trainer = trainer,
                Groups = _uow.TrainerGroupRepository.List()
            });
        }

        [HttpPost("[area]/[controller]/[action]/{id:guid}")]
        public IActionResult Edit(EditTrainerViewModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var trainer = _uow.TrainerRepository.Find(model.Trainer.Id);
            if (trainer == null) return NotFound();

            var group = new TrainerGroup();
            if (model.Trainer.TrainerGroupId != null) group = GetGroupById(model.Trainer.TrainerGroupId.Value);

            trainer.TrainerGroup = group;
            trainer.TrainerGroupId = group.Id;
            trainer.VisualOrder = model.Trainer.VisualOrder;
            trainer.Description = model.Trainer.Description;

            _uow.Commit();
            return RedirectToAction("List");
        }

        public TrainerGroup GetGroupById(Guid id)
        {
            return _uow.TrainerGroupRepository.List().FirstOrDefault(gr => gr.Id.CompareTo(id) == 0);
        }

        public User GetUserById(Guid id)
        {
            return _uow.UserRepository.List().FirstOrDefault(u => u.Id.CompareTo(id) == 0);
        }

        [HttpPost("[action]/{id:guid}")]
        public IActionResult Delete(Guid id)
        {
            var trainer = _uow.TrainerRepository.Find(id);
            if (trainer == null)
                return NotFound();

            _uow.TrainerRepository.Remove(trainer);
            _uow.Commit();

            return RedirectToAction("List");
        }

        [HttpGet("[area]/[controller]/[action]")]
        public IActionResult Add()
        {
            return View(new AddTrainerViewModel
            {
                Trainer = new Trainer(),
                UserId = Guid.Empty,
                Groups = _uow.TrainerGroupRepository.List(),
                Users = GetExtraUsers()
            });
        }

        [HttpPost("[area]/[controller]/[action]")]
        public IActionResult Add(AddTrainerViewModel model)
        {
            if (!ModelState.IsValid) return View(model);

            var trainer = new Trainer();

            var group = new TrainerGroup();
            if (model.Trainer.TrainerGroupId != null) group = GetGroupById(model.Trainer.TrainerGroupId.Value);

            var user = new User();
            if (model.UserId != Guid.Empty) user = GetUserById(model.UserId);

            trainer.Id = Guid.NewGuid();
            trainer.User = user;
            trainer.TrainerGroupId = group.Id;
            trainer.TrainerGroup = group;
            trainer.VisualOrder = model.Trainer.VisualOrder;
            trainer.Description = model.Trainer.Description;

            _uow.TrainerRepository.Add(trainer);
            _uow.Commit();
            return RedirectToAction("List");
        }

        public IEnumerable<User> GetExtraUsers()
        {
            var users = new List<User>();
            users.AddRange(_uow.UserRepository.ActiveUsers());
            var count = _uow.TrainerRepository.GetCount();
            var trainers = _uow.TrainerRepository.List().ToList();
            for (var i = 0; i < count; i++) users.Remove(trainers.ElementAt(i).User);

            return users;
        }
    }
}