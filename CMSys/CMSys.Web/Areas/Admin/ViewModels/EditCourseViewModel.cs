﻿using System.Collections.Generic;
using CMSys.Core.Entities;

namespace CMSys.Web.Areas.Admin.ViewModels
{
    public class EditCourseViewModel
    {
        public Course Course { get; set; }
        public IEnumerable<CourseGroup> Groups { get; set; }
        public IEnumerable<CourseType> Types { get; set; }
    }
}