﻿using System.Collections.Generic;
using CMSys.Core.Entities;

namespace CMSys.Web.Areas.Admin.ViewModels
{
    public class CourseGroupViewModel
    {
        public CourseGroup CourseGroup { get; set; }
        public IEnumerable<CourseGroup> Groups { get; set; }
    }
}
