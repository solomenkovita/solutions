﻿using System;
using System.Collections.Generic;
using CMSys.Core.Entities;

namespace CMSys.Web.Areas.Admin.ViewModels
{
    public class AddTrainerViewModel
    {
        public Guid UserId { get; set; }
        public Trainer Trainer { get; set; }
        public IEnumerable<TrainerGroup> Groups { get; set; }
        public IEnumerable<User> Users { get; set; }
    }
}