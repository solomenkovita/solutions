﻿using System.Collections.Generic;
using CMSys.Core.Entities;

namespace CMSys.Web.Areas.Admin.ViewModels
{
    public class EditTrainerViewModel
    {
        public Trainer Trainer { get; set; }
        public IEnumerable<TrainerGroup> Groups { get; set; }
    }
}
