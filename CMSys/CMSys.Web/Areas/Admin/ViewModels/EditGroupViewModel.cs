﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CMSys.Web.Areas.Admin.ViewModels
{
    public class EditGroupViewModel
    {
        public Guid Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        public string Description { get; set; }
        [Required(ErrorMessage = "Order is required")]
        public int VisualOrder { get; set; }
    }
}
