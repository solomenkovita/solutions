﻿using System;
using System.Collections.Generic;
using CMSys.Core.Entities;

namespace CMSys.Web.Areas.Admin.ViewModels
{
    public class EditCourseTrainersViewModel
    {
        public Guid TrainerId { get; set; }
        public Course Course { get; set; }
        public IEnumerable<Trainer> Trainers { get; set; }
    }
}
