﻿using System.Collections.Generic;
using CMSys.Core.Entities;

namespace CMSys.Web.Areas.Admin.ViewModels
{
    public class TrainerGroupViewModel
    {
        public TrainerGroup TrainerGroup { get; set; }
        public IEnumerable<TrainerGroup> Groups { get; set; }
    }
}
