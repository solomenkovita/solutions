﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using CMSys.Core.Entities;

namespace CMSys.Web.Areas.Admin.ViewModels
{
    public class EditUserViewModel
    {
        public int Page { get;set; }
        public User User { get; set; }

        public string NewPassword { get; set; }

        [Compare(nameof(NewPassword), ErrorMessage = "Passwords do not match")]
        public string RepeatPassword { get; set; }

        public IEnumerable<Role> Roles { get; set; }
        public Guid Role { get; set; }
    }
}
