﻿using CMSys.Core.Common;
using CMSys.Core.Entities;
using CMSys.Web.ViewModels;

namespace CMSys.Web.Areas.Admin.ViewModels
{
    public class UserViewModel
    {
        public PageViewModel<User> PageModel { get; set; }
        public UserFilter UserFilter { get; set; }
        public string Name { get; set; }
    }
}
