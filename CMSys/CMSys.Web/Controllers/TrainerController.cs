﻿using CMSys.Core.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Controllers
{
    [Authorize(Roles = "User,Admin")]
    [Route("user/[controller]")]
    public class TrainerController : Controller
    {
        private readonly IUnitOfWork _uow;

        public TrainerController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [Route("")]
        [Route("[controller]/[action]")]
        public IActionResult List(int page = 1)
        {
            return View(_uow.TrainerRepository.Groups());
        }
    }
}