﻿using System;
using System.Threading.Tasks;
using CMSys.Core.Common;
using CMSys.Core.Entities;
using CMSys.Core.Repositories;
using CMSys.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Controllers
{
    [Authorize(Roles = "User,Admin")]
    [Route("user/[controller]")]
    public class CourseController : Controller
    {
        private readonly IUnitOfWork _uow;

        public CourseController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [Route("")]
        [Route("[controller]/[action]")]
        public async Task<IActionResult> List(int page = 1, Guid groupId = default, Guid typeId = default)
        {
            var courseFilter = new CourseFilter(groupId, typeId, page);
            var tuple = await _uow.CourseRepository.GetPageItems(courseFilter);
            return View(new CourseViewModel
            {
                PageModel = PageViewModel<Course>.Create(tuple.Item1, tuple.Item2, page, courseFilter.Size, groupId, typeId),
                Groups = _uow.CourseGroupRepository.List(),
                Types = _uow.CourseTypeRepository.List(),
                CurrentType = typeId,
                CurrentGroup = groupId
            });
        }

        [Route("[controller]/[action]/{id:guid}")]
        public IActionResult Details(Guid id)
        {
            var course = _uow.CourseRepository.Find(id);
            return course == null ? (IActionResult) NotFound() : View(course);
        }
    }
}