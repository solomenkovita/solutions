﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Controllers
{
    [Authorize(Roles = "User,Admin")]
    [Route("user/home")]
    public class HomeController : Controller
    {
        [Route("")]
        [Route("Index")]
        public IActionResult Index()
        {   
            return View();
        }
    }
}
