﻿using System;
using CMSys.Core.Common;
using CMSys.Core.Repositories;
using CMSys.Web.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Controllers
{
    [Authorize(Roles = "User,Admin")]
    [Route("user/[controller]")]
    public class UserDetailsController : Controller
    {
        private readonly IUnitOfWork _uow;

        public UserDetailsController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [Route("[action]")]
        public IActionResult Details(string email)
        {
            var user = _uow.UserRepository.FindByEmail(email);
            return user == null ? (IActionResult)NotFound() : View(user);
        }

        [HttpGet("[action]/{id:guid}")]
        public IActionResult ChangePassword(Guid id)
        {
            var user = _uow.UserRepository.Find(id);
            if (user == null)
            {
                return NotFound();
            }

            return View(new ChangePasswordViewModel
            {
                Id = user.Id
            });
        }

        [HttpPost("[action]/{id:guid}")]
        public IActionResult ChangePassword(ChangePasswordViewModel model)
        {
            var user = _uow.UserRepository.Find(model.Id);
            if (user == null) return NotFound();
            if (!ModelState.IsValid)
            {
                model.Id = user.Id;
                return View(model);
            }

            user.PasswordSalt = Guid.NewGuid().ToString();
            user.PasswordHash = UserPasswords.ComputeHash(model.NewPassword, user.PasswordSalt);

            _uow.Commit();
            return RedirectToAction("Index","Home");
        }
    }
}
