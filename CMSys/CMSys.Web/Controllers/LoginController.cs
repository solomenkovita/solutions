﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using CMSys.Core.Repositories;
using CMSys.Web.ViewModels;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;

namespace CMSys.Web.Controllers
{
    public class LoginController : Controller
    {
        private readonly IUnitOfWork _uow;

        public LoginController(IUnitOfWork uow)
        {
            _uow = uow;
        }

        [Route("")]
        [Route("[controller]/[action]")]
        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        [Route("[controller]/[action]")]
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = _uow.UserRepository.FindByEmail(model.Email);
            if (user == null)
            {
                ModelState.AddModelError("Email", "Invalid email");
            }

            if (user.CheckPassword(model.Password) != true)
            {
                ModelState.AddModelError("Password", "Invalid password.");
            }

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.FirstName + " " + user.LastName),
                new Claim(ClaimTypes.Email, user.Email)
            };

            var roles = user.UserRole.Select(x => new Claim(ClaimTypes.Role, x.Role.Name)).ToList();
            claims.AddRange(roles);

            var identity = new ClaimsIdentity(claims, "cookie");
            var principal = new ClaimsPrincipal(identity);

            await HttpContext.SignInAsync("cookie", principal);
            return RedirectToAction("Index", "Home");
        }

        [Route("[controller]/[action]")]
        public async Task<ActionResult> Logout()
        {
            await HttpContext.SignOutAsync("cookie");
            return RedirectToAction("Login");
        }
    }
}
