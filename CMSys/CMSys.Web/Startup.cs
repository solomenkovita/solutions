using CMSys.Core.Repositories;
using CMSys.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace CMSys.Web
{
    public class Startup
    {

        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddDbContext<CMSysContext>(option =>
                option.UseSqlServer(Configuration.GetConnectionString("CMSys")));
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddAuthentication("cookie").AddCookie("cookie", options =>
            {
                options.LoginPath = new PathString("/Login/Login");
                options.ReturnUrlParameter = "returnUrl";
            });
        }

        public void Configure(IApplicationBuilder app)
        {
            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints => { endpoints.MapControllers();
            });
        }
    }
}
