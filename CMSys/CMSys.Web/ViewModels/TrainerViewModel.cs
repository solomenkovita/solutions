﻿using System;

namespace CMSys.Web.ViewModels
{
    public class TrainerViewModel
    {   
        public Guid? Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Description { get; set; }
        public byte[] Photo { get; set; }
    }
}
