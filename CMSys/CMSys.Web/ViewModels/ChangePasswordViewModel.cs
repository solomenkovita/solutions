﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CMSys.Web.ViewModels
{
    public class ChangePasswordViewModel
    {
        public Guid Id { get; set; }
        public string NewPassword { get; set; }
        [Compare(nameof(NewPassword), ErrorMessage = "Passwords do not match")]
        public string RepeatPassword { get; set; }
    }
}
