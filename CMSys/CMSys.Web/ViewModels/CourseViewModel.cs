﻿using System;
using System.Collections.Generic;
using CMSys.Core.Entities;

namespace CMSys.Web.ViewModels
{
    public class CourseViewModel
    {
        public PageViewModel<Course> PageModel { get; set; }
        public IEnumerable<CourseGroup> Groups { get; set; }
        public IEnumerable<CourseType> Types { get; set; }
        public Guid CurrentType { get; set; }
        public Guid CurrentGroup { get; set; }
    }
}
