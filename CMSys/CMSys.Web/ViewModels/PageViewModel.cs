﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CMSys.Web.ViewModels
{
    public class PageViewModel<T> : List<T>
    {
        public int PageNumber { get; private set; }
        public int TotalPages { get; private set; }
        public Guid Group { get; private set; }
        public Guid Type { get; private set; }

        public PageViewModel(List<T> items, int count, int pageNumber, int pageSize, Guid group, Guid type)
        {
            PageNumber = pageNumber;
            TotalPages = (int)Math.Ceiling(count / (double)pageSize);
            this.AddRange(items);
            Group = group;
            Type = type;
        }

        public bool HasPreviousPage => (PageNumber > 1);

        public bool HasNextPage => (PageNumber < TotalPages);

        public static PageViewModel<T> Create(IEnumerable<T> source, int count, int pageNumber, int pageSize, Guid group, Guid type)
        {
            var items =  source.ToList();
            return new PageViewModel<T>(items, count, pageNumber, pageSize, group, type);
        }
    }
}
