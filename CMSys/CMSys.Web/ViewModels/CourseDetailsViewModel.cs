﻿using CMSys.Core.Entities;

namespace CMSys.Web.ViewModels
{
    public class CourseDetailsViewModel
    {
        public CourseViewModel CourseViewModel { get; set; }
        public Course Course { get; set; }
    }
}
