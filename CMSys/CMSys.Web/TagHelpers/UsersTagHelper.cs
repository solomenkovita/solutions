﻿using System.Collections.Generic;
using System.Linq;
using CMSys.Core.Entities;
using CMSys.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace CMSys.Web.TagHelpers
{
    public class UsersTagHelper : TagHelper
    {
        private IUrlHelperFactory _urlHelperFactory;

        public UsersTagHelper(IUrlHelperFactory helperFactory)
        {
            _urlHelperFactory = helperFactory;
        }

        [ViewContext] [HtmlAttributeNotBound] public ViewContext ViewContext { get; set; }
        public PageViewModel<User> PageModel { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Area { get; set; }

        [HtmlAttributeName(DictionaryAttributePrefix = "page-url-")]
        public Dictionary<string, object> PageUrlValues { get; set; } = new Dictionary<string, object>();

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            var urlHelper = _urlHelperFactory.GetUrlHelper(ViewContext);
            output.TagName = "nav";

            var tag = new TagBuilder("ul");
            tag.AddCssClass("pagination");

            var items = new List<TagBuilder>();

            var prev = CreateItem(PageModel.PageNumber - 1, urlHelper, "Previous");
            if (!PageModel.HasPreviousPage)
                prev.AddCssClass("disabled");
            items.Add(prev);

            if (PageModel.TotalPages <= 6)
            {
                items.AddRange(Enumerable.Range(1, PageModel.TotalPages).Select(x => CreateItem(x, urlHelper)));
            }
            else if (PageModel.PageNumber <= 3)
            {
                items.AddRange(Enumerable.Range(1, 4).Select(x => CreateItem(x, urlHelper)));
                items.Add(CreateItem(null, urlHelper, "..."));
                items.Add(CreateItem(PageModel.TotalPages, urlHelper));
            }
            else if (PageModel.TotalPages - PageModel.PageNumber <= 3)
            {
                items.Add(CreateItem(1, urlHelper));
                items.Add(CreateItem(null, urlHelper, "..."));
                items.AddRange(Enumerable.Range(PageModel.TotalPages - 3, 4).Select(x => CreateItem(x, urlHelper)));
            }
            else
            {
                items.Add(CreateItem(1, urlHelper));
                if (PageModel.PageNumber > 4)
                    items.Add(CreateItem(null, urlHelper, "..."));

                items.AddRange(Enumerable.Range(PageModel.PageNumber - 2, 4).Select(x => CreateItem(x, urlHelper)));

                if (PageModel.PageNumber < PageModel.TotalPages - 3)
                    items.Add(CreateItem(null, urlHelper, "..."));
                items.Add(CreateItem(PageModel.TotalPages, urlHelper));
            }

            var next = CreateItem(PageModel.PageNumber + 1, urlHelper, "Next");
            if (!PageModel.HasNextPage)
                next.AddCssClass("disabled");
            items.Add(next);

            foreach (var item in items)
                tag.InnerHtml.AppendHtml(item);
            output.Content.AppendHtml(tag);
        }

        private TagBuilder CreateItem(int? page, IUrlHelper urlHelper, string text = null)
        {
            var tag = new TagBuilder("li");
            tag.AddCssClass("page-item");

            var link = new TagBuilder("a");
            link.AddCssClass("page-link");

            if (!page.HasValue)
                tag.AddCssClass("disabled");
            else if (page.Value == PageModel.PageNumber)
                tag.AddCssClass("active");
            else if (string.IsNullOrEmpty(Area))
                link.Attributes["href"] = urlHelper.Action(Action, Controller, new {page = page.Value});
            else link.Attributes["href"] = urlHelper.Action(Action, Controller, new {area = Area, page = page.Value});

            link.InnerHtml.AppendHtml(text ?? page.ToString());
            tag.InnerHtml.AppendHtml(link);

            return tag;
        }
    }
}