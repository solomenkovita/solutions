﻿using System;
using System.Collections.Generic;

namespace CMSys.Core.Entities
{
    public sealed class Role : Entity<Guid>
    {
        public Role()
        {
            UserRole = new HashSet<UserRole>();
        }

        public string Name { get; set; }

        public ICollection<UserRole> UserRole { get; set; }
    }
}