﻿using System;
using System.Collections.Generic;

namespace CMSys.Core.Entities
{
    public sealed class CourseType : Entity<Guid>
    {
        public string Name { get; set; }
        public int VisualOrder { get; set; }
        public string Description { get; set; }

        public ICollection<Course> Course { get; set; }
    }
}