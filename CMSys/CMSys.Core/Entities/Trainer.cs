﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CMSys.Core.Entities
{
    public sealed class Trainer : Entity<Guid>
    {
        public Trainer()
        {
            CourseTrainers = new HashSet<CourseTrainer>();
        }

        [DefaultValue(0)]
        [Required(ErrorMessage = "Order is required")]
        public int VisualOrder { get; set; }

        public string Description { get; set; }

        public Guid? TrainerGroupId { get; set; }
        public TrainerGroup TrainerGroup { get; set; }

        public User User { get; set; }

        public ICollection<CourseTrainer> CourseTrainers { get; set; }

        public override string ToString()
        {
            return User.ToString();
        }
    }
}