﻿using System;

namespace CMSys.Core.Entities
{
    public sealed class UserRole : Entity
    {
        public Guid UserId { get; set; }
        public User User { get; set; }

        public Guid RoleId { get; set; }
        public Role Role { get; set; }
    }
}
