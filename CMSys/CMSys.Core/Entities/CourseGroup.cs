﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CMSys.Core.Entities
{
    public sealed class CourseGroup : Entity<Guid>
    {
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Order is required")]
        public int VisualOrder { get; set; }

        public string Description { get; set; }

        public ICollection<Course> Course { get; set; }
    }
}