﻿using System;
using System.Collections.Generic;
using CMSys.Core.Common;

namespace CMSys.Core.Entities
{
    public sealed class User : Entity<Guid>
    {
        public User()
        {
            UserRole = new HashSet<UserRole>();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public string PasswordSalt { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Department { get; set; }
        public string Position { get; set; }
        public string Location { get; set; }
        public byte[] Photo { get; set; }

        public ICollection<UserRole> UserRole { get; set; }

        public override string ToString()
        {
            return FirstName + LastName;
        }

        public bool CheckPassword(string password)
        {
            return !string.IsNullOrEmpty(password) &&
                   UserPasswords.ComputeHash(password, password).Equals(PasswordHash);
        }
    }
}