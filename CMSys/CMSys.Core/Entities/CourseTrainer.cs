﻿using System;
using System.Collections.Generic;

namespace CMSys.Core.Entities
{
    public sealed class CourseTrainer : Entity
    {
        public Guid? CourseId { get; set; }
        public Course Course { get; set; }

        public Guid? TrainerId { get; set; }
        public Trainer Trainer { get; set; }
        public int VisualOrder { get; set; }

    }
}
