﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CMSys.Core.Entities
{
    public sealed class Course : Entity<Guid>
    {
        public Course()
        {
            CourseTrainers = new HashSet<CourseTrainer>();
        }

        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Order is required")]
        public int VisualOrder { get; set; }

        public bool IsNew { get; set; }
        public string Description { get; set; }

        public Guid? CourseGroupId { get; set; }
        public CourseGroup CourseGroup { get; set; }

        public Guid? CourseTypeId { get; set; }
        public CourseType CourseType { get; set; }

        public ICollection<CourseTrainer> CourseTrainers { get; set; }
    }
}