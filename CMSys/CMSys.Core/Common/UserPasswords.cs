﻿using System;

namespace CMSys.Core.Common
{
    public class UserPasswords
    {
        public static string ComputeHash(string password, string salt)
        {
            Check(password);
            Check(salt);

            var hashAlgorithm = new Sha512Hash();
            return hashAlgorithm.CalculateHash(password + salt);
        }

        public static void Check(string obj)
        {
            if (string.IsNullOrEmpty(obj)) throw new ArgumentException(nameof(obj));
        }
    }
}
