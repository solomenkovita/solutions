﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CMSys.Core.Common
{
    public class UserFilter
    {
        public int Page { get; set; }
        public int Start { get; set; }
        public int Num { get; set; }
        public int Count { get; set; }
        public int First { get; set; }
        public int Size => 10;

        public UserFilter(int page)
        {
            Page = page;
            Start = page * Size - Size;
            Num = Start + Size;
            First = Start + 1;
        }
    }
}
