﻿using System;
using System.Linq.Expressions;
using CMSys.Core.Entities;

namespace CMSys.Core.Common
{
    public class CourseFilter
    {
        public Expression<Func<Course, bool>> Filter { get; set; }
        public Guid GroupId { get; set; }
        public Guid TypeId { get; set; }
        public int Page { get; set; }
        public int Start { get; set; }
        public int Size => 12;

        public CourseFilter(Guid groupId, Guid typeId, int page)
        {
            GroupId = groupId;
            TypeId = typeId;
            Page = page;
            Start = page * Size - Size;
            GetFilter();
        }

        public void GetFilter()
        {
            if (GroupId == default && TypeId != default)
                Filter = x => x.CourseTypeId == TypeId;
            if (GroupId != default && TypeId == default)
                Filter = x => x.CourseGroupId == GroupId;
            if (GroupId != default && TypeId != default)
                Filter = x => x.CourseGroupId == GroupId && x.CourseTypeId == TypeId;
        }
    }
}
