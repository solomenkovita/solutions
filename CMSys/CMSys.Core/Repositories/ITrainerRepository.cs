﻿using System;
using System.Collections.Generic;
using System.Linq;
using CMSys.Core.Entities;

namespace CMSys.Core.Repositories
{
    public interface ITrainerRepository : IRepository<Trainer, Guid>
    {
        public IEnumerable<IGrouping<string, Trainer>> Groups();
        public int GetCount();
    }
}
