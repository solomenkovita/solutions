﻿using System;
using CMSys.Core.Entities;

namespace CMSys.Core.Repositories
{
    public interface ICourseGroupRepository : IRepository<CourseGroup, Guid>
    {
    }
}
