﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CMSys.Core.Common;
using CMSys.Core.Entities;

namespace CMSys.Core.Repositories
{
    public interface IUserRepository : IRepository<User, Guid>
    {
        public IEnumerable<User> ActiveUsers();
        public Task<(IEnumerable<User>, int)> GetPageItems(UserFilter filter, string name);
        public User FindByEmail(string email);
    }
}
