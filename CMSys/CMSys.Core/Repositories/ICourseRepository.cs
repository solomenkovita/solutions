﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CMSys.Core.Common;
using CMSys.Core.Entities;

namespace CMSys.Core.Repositories
{
    public interface ICourseRepository : IRepository<Course, Guid>
    {
        public Task<(IEnumerable<Course>, int)> GetPageItems(CourseFilter courseFilter);
    }
}
