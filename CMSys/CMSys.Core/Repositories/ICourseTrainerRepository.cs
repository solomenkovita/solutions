﻿using System;
using CMSys.Core.Entities;

namespace CMSys.Core.Repositories
{
    public interface ICourseTrainerRepository
    {
        CourseTrainer Find(Guid trainerId, Guid courseId);
    }
}
