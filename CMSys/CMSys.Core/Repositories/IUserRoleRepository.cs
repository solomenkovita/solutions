﻿using System;
using CMSys.Core.Entities;

namespace CMSys.Core.Repositories
{
    public interface IUserRoleRepository
    {
        void Add(UserRole entity);
        void Remove(UserRole entity);
        public UserRole Find(Guid userId, Guid roleId);
    }
}
