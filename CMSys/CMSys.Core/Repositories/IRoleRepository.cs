﻿using System;
using CMSys.Core.Entities;

namespace CMSys.Core.Repositories
{
    public interface IRoleRepository : IRepository<Role, Guid>
    {
    }
}
