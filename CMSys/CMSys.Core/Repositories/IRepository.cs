﻿using CMSys.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace CMSys.Core.Repositories
{
    public interface IRepository<TEntity, in TKey> where TEntity : Entity<TKey>
    {
        TEntity Find(TKey id);
        IEnumerable<TEntity> List(Expression<Func<TEntity, bool>> predicate = null);

        void Add(TEntity entity);
        void Remove(TEntity entity);
    }
}
