﻿using System;
using CMSys.Core.Entities;

namespace CMSys.Core.Repositories
{
    public interface ITrainerGroupRepository : IRepository<TrainerGroup, Guid>
    {
    }
}
