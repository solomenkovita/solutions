﻿using System;
using CMSys.Core.Entities;

namespace CMSys.Core.Repositories
{
    public interface ICourseTypeRepository : IRepository<CourseType, Guid>
    {
    }
}
