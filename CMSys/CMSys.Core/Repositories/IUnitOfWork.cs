﻿using System;

namespace CMSys.Core.Repositories
{
    public interface IUnitOfWork : IDisposable
    {
        ICourseGroupRepository CourseGroupRepository { get; }
        ICourseRepository CourseRepository { get; }
        ICourseTrainerRepository CourseTrainerRepository { get; }
        ICourseTypeRepository CourseTypeRepository { get; }
        IRoleRepository RoleRepository { get; }
        ITrainerRepository TrainerRepository { get; }
        ITrainerGroupRepository TrainerGroupRepository { get; }
        IUserRepository UserRepository { get; }
        IUserRoleRepository UserRoleRepository { get; }

        void Commit();
    }
}
